package tom.utils.memory

import sun.misc.Unsafe
import tom.utils.memory.RCMemoryBlock.Companion.allocate


/**
 * @author Thomas Povinelli
 * Created 10/10/19
 * In TomUtils
 */

@PublishedApi
internal val unsafe: Unsafe
    get() = checkNotNull(Unsafe::class.java.getDeclaredField("theUnsafe")?.run {
        isAccessible = true
        return@run get(null) as? Unsafe
    }) { "Could not access Unsafe instance " }

/**
 * Represents a manually reference-counted block of native memory.
 *
 * This represents a block of memory allocated by [sun.misc.Unsafe] that is
 * reference counted. Calling [retain] increases the reference count by 1
 * and [release] decrements the reference count. When a call to [release]
 * causes the reference count to be lowered to 0, the memory is freed using
 * Unsafe. This means calls to [retain] and [release] must be balanced. In
 * actuality, calls are near-balanced (See below).
 *
 * After calling [allocate] or [allocateZeroed] the reference count begins at 1.
 * This means all but 1 calls to [release] must be balanced by a call to
 * [retain]
 *
 * These blocks are always byte-typed and byte addressable. Access is done
 * by indexing. Indexes start at 0 and go to the size exclusive.
 * When using a method that takes two indexes such as [get] or [set] the
 * end passed is not inclusive so it is valid to call get(0, size) since size
 * is not inclusive. However it is not valid to call get(size) as this is an overflow
 * The get method can take one index and return a byte or a range of indexes
 * and return a byte array. The set method can set a single byte to a value
 * a range of bytes to a single value or a range of bytes to multiple values in
 * order
 */
class RCMemoryBlock private constructor(val address: Long, val size: Long) {

    private var refCount: Int = 1
    private val paddr: String
        get() = "0x${address.toString(16)}"

    companion object {
        /**
         * Allocates [size] bytes in Native Memory returning a NativeMemory
         * object which has an address, size, and refCount and can be
         * [retain]ed and [release]ed
         *
         * Memory returned by [allocate] will not be initialized. Call [zero]
         * to clear memory after allocation
         */
        fun allocate(size: Long): RCMemoryBlock {
            require(size > 0) { "can only allocate a positive number of bytes not $size" }
            val address = unsafe.allocateMemory(size)
            return RCMemoryBlock(address, size)
        }
    }

    @Suppress("NOTHING_TO_INLINE")
    private inline fun boundsCheck(start: Long, end: Long) {
        require(start >= 0) { "starting index must be positive or 0, not $start" }
        require(start < end) { "end ($end) is exclusive and must be > start ($start)" }
        require(end <= size) { "end ($end) is exclusive and must be <= size ($size)" }
    }

    operator fun get(at: Long): Byte {
        require(at in 0 until size) { "index must be 0 <= index < $size. Got $at" }
        return unsafe.getByte(address + at)
    }

    /**
     * [end] is exclusive. A NativeMemory block, n,  of size 13 can be accessed all
     * at once by saying n[0, 13]
     */
    operator fun get(start: Long, end: Long): ByteArray {
        boundsCheck(start, end)
        val request = end - start
        val size = request.toInt()
        require(request == size.toLong()) { "Resource at $paddr: get range too big to fit in ByteArray" }
        val arr = ByteArray(size)
        for (i in start until end)
            arr[(i - start).toInt()] = this[i]
        return arr
    }

    operator fun set(at: Long, value: Byte) {
        require(at in 0 until size) { "index must be 0 <= index < $size. Got $at" }
        unsafe.putByte(address + at, value)
    }

    /**
     * [end] is exclusive, [start] is inclusive
     */
    operator fun set(start: Long, end: Long, value: Byte) {
        boundsCheck(start, end)
        for (i in start until end)
            this[i] = value
    }

    /**
     * [end] is exclusive, [start] is inclusive
     */
    operator fun set(start: Long, end: Long, values: ByteArray) {
        boundsCheck(start, end)
        require(end - start == values.size.toLong()) {
            "ByteArray must be same size as range being set " +
              "[values.size (${values.size} != index range (${end - start})]"
        }
        var i = 0
        for (p in start until end)
            this[p] = values[i++]
    }

    private fun free() {
        unsafe.freeMemory(address)
    }

    /**
     * Increases the reference count of this block of memory
     */
    fun retain() {
        check(refCount != 0) { "Resource at $paddr already freed before calling retain" }
        refCount++
    }

    /**
     * Decreases the reference count of this block of memory and, if necessary,
     * frees it
     */
    fun release() {
        check(refCount > 0) { "Resource at $paddr released more times than retained" }
        refCount--
        if (refCount == 0) {
            println("Freeing memory of size $size at $paddr")
            free()
        }
    }
}

val RCMemoryBlock.all: ByteArray get() = get(0, size)

/**
 * Sets all bytes in the block of memory to 0. Returns `this` so call can be
 * chained after [allocate] such as
 *
 * `val block = RCMemoryBlock.allocate(16).zero()
 */
fun RCMemoryBlock.zero(): RCMemoryBlock {
    for (i in 0 until size)
        this[i] = 0
    return this
}

inline fun <R> RCMemoryBlock.auto(block: (RCMemoryBlock) -> R): R {
    retain()
    try {
        return block(this)
    } finally {
        release()
    }
}

inline fun <R> RCMemoryBlock.Companion.tidy(size: Long, block: (RCMemoryBlock) -> R): R {
    val memory = allocate(size)
    try {
        return block(memory)
    } finally {
        memory.release()
    }
}
