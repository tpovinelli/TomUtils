package tom.utils.aggregation

import tom.utils.string.plusAssign
import java.io.Serializable
import java.math.BigInteger
import java.util.*
import java.util.stream.Stream
import kotlin.experimental.or

/**
 * This class represents a BitVector which is a sequence of
 * single bits. It functions similarly to an Array.
 *
 * @implDetail this is implemented using a backing byte array
 *             since Java does not allow bit access to data types
 *             As such any sizes provided to the constructor
 *             are rounded up to the next multiple of 8
 *             All bit vectors are sized by some multiple of 8
 *             This means if you request a bit vector of bitsSize 25,
 *             you will get one of bitsSize 32. The [BitVector.requestedSize]
 *             variable can be used to determine the original requested bitsSize
 *
 * @implDetail because this uses an array for backing, BitVector
 *             functions like an array. It has get and set methods
 *             but no add methods. Adding should be done by
 *             setting the next available index and resizing
 *             the BitVector as needed. Because of the array implementation
 *             BitVector implements [RandomAccess]
 */
class BitVector(size: Int) : Serializable, RandomAccess, Collection<Boolean> {

    override val size: Int
        get() = requestedSize

    override fun contains(element: Boolean): Boolean {
        for (e in this) {
            if (e == element) {
                return true
            }
        }
        return false
    }

    override fun containsAll(elements: Collection<Boolean>): Boolean {
        return elements.all { contains(it) }
    }

    override fun isEmpty(): Boolean {
        return size == 0
    }

    enum class MSBBit {
        MSB_FIRST_BIT, MSB_LAST_BIT, NONE
    }

    private val backing: ByteArray = ByteArray(size / 8 + 1)
    val requestedSize: Int = size

    fun combine(
        other: BitVector,
        msb: MSBBit = MSBBit.NONE,
        transform: (Boolean, Boolean) -> Boolean,
        default: (BitVector, Int) -> Boolean
    ): BitVector {
        val larger = if (other.bitsSize > bitsSize) other else this
        when (msb) {
            MSBBit.MSB_LAST_BIT -> return BitVector(other.bitsSize) {
                if (it < bitsSize && it < other.bitsSize) {
                    transform(this[it], other[it])
                } else {
                    // in MSB_LAST_BIT mode the 0th index corresponds to the LSB
                    // meaning additional bits beyond the end of one vector count as
                    // the MSB and the shorter BitVector is padded with 0
                    default(larger, it)
                }
            }

            MSBBit.MSB_FIRST_BIT -> {
                // TODO: Yikes
                val tempThis = reversed()
                val tempThat = other.reversed()
                return combine(other, MSBBit.MSB_LAST_BIT, transform, default)
            }

            else -> {
                if (bitsSize != other.bitsSize) {
                    throw IllegalArgumentException("Specify a MSB to operate on BitVectors of unequal length")
                }
                return combine(other, MSBBit.MSB_LAST_BIT, transform, default)
            }
        }
    }

    fun and(other: BitVector, msb: MSBBit = MSBBit.NONE): BitVector {
        return combine(other, msb, { a, b -> a && b }, { v, i -> false })
    }

    fun or(other: BitVector, msb: MSBBit = MSBBit.NONE): BitVector {
        return combine(other, msb, { a, b -> a || b }, { v, i -> v[i] })
    }

    fun xor(other: BitVector, msb: MSBBit = MSBBit.NONE): BitVector {
        // default is 0 padded, 0 xor
        return combine(other, msb, Boolean::xor) { v, i -> v[i] }
    }


    val bitsSize: Int
        get() {
            return backing.size * 8
        }

    /**
     * This constructor can be used to copy or to grow a BitVector
     * for example to copy
     * val b = BitVector(10) { Math.random() < 0.5 }
     * val copy = BitVector(b.bitsSize, b)
     *
     * or to grow
     * val c = BitVector(10) { Math.random() > 0.25 }
     * val largerC = BitVector(c.bitsSize * 2, c)
     */
    constructor(newSize: Int, initialValues: BitVector) : this(newSize) {
        for (idx in 0 until initialValues.bitsSize) {
            this[idx] = initialValues[idx]
        }
    }

    constructor(size: Int, init: (Int) -> Boolean) : this(size) {
        for (idx in 0 until size) {
            this[idx] = init(idx)
        }
    }

    // for Java
    constructor(size: Int, init: java.util.function.Function<Int, Boolean>) : this(size) {
        for (idx in 0 until size) {
            this[idx] = init.apply(idx)
        }
    }

    operator fun get(idx: Int): Boolean {
        val backingIdx = idx / 8
        val bit = idx % 8
        val byte = backing[backingIdx]
        val result = (byte.toInt() shr bit) and 1
        return result == 1
    }

    operator fun set(idx: Int, value: Boolean) {
        val backingIdx = idx / 8
        val bit = idx % 8
        if (value) {
            backing[backingIdx] = backing[backingIdx] or (1 shl bit).toByte()
        } else {
            backing[backingIdx] = (backing[backingIdx].toInt() and (0b11111111 xor (1 shl bit))).toByte()
        }
    }

    fun toBinaryNumber(): String {
        return toBitString().reversed()
    }

    override fun toString(): String {
        if (bitsSize == 0) {
            return "[]"
        }
        val str = StringBuilder("[")
        for (i in 0 until requestedSize) {
            str += (this[i].toString() + ", ")
        }
        str.delete(str.length - 2, str.length)
        str += "]"
        return str.toString()
    }

    fun stream(): Stream<Boolean> {
        return Arrays.stream(toBooleanArray().toTypedArray())
    }

    val trueCount: Int
        get() = toBooleanArray().fold(0) { acc: Int, b: Boolean -> if (b) acc + 1 else acc }

    val falseCount: Int
        get() = toBooleanArray().fold(0) { acc: Int, b: Boolean -> if (b) acc else acc + 1 }

    fun toBitString(): String {
        var s = ""
        for (ele in this) {
            s += if (ele) {
                "1"
            } else {
                "0"
            }
        }
        return s
    }

    fun toBooleanArray(): BooleanArray = BooleanArray(bitsSize) {
        this[it]
    }

    fun toBigInteger(): BigInteger {
        backing.reverse()
        val ret = BigInteger(backing)
        backing.reverse()
        return ret
    }

    override operator fun iterator(): Iterator<Boolean> {
        return object : Iterator<Boolean> {
            var cIdx = 0
            override fun hasNext(): Boolean {
                return cIdx < bitsSize
            }

            override fun next(): Boolean {
                return this@BitVector[cIdx++]
            }
        }
    }
}
