package tom.utils.aggregation

/**
 * @author Thomas Povinelli
 * Created 5/30/17
 * In TomUtils
 */
import java.io.Serializable

operator fun <E> List<E>.component1(): E {
    return first()
}

operator fun <E> List<E>.component2(): List<E> {
    return drop(1)
}

data class Quadruple<out A, out B, out C, out D>(val first: A, val second: B, val third: C, val fourth: D) :
    Serializable

data class Quintuple<out A, out B, out C, out D, out E>(
    val first: A,
    val second: B,
    val third: C,
    val fourth: D,
    val fifth: E
) : Serializable

data class Sextuple<out A, out B, out C, out D, out E, out F>(
    val first: A, val second: B, val third: C, val fourth: D, val fifth: E,
    val sixth: F
) : Serializable

data class Septuple<out A, out B, out C, out D, out E, out F, out G>(
    val first: A, val second: B, val third: C, val fourth: D, val fifth: E, val sixth: F,
    val seventh: G
) : Serializable


// Single Element to Pair
infix fun <A, B> A.to(o: B): Pair<A, B> {
    return Pair(this, o)
}

// Pair to Triple
infix fun <A, B, C> Pair<A, B>.to(o: C): Triple<A, B, C> {
    return Triple(this.first, this.second, o)
}

infix fun <A, B, C> A.to(o: Pair<B, C>): Triple<A, B, C> {
    return Triple(this, o.first, o.second)
}

// Triple to Quadruple
infix fun <A, B, C, D> Triple<A, B, C>.to(o: D): Quadruple<A, B, C, D> {
    return Quadruple(this.first, this.second, this.third, o)
}

infix fun <A, B, C, D> Pair<A, B>.to(o: Pair<C, D>): Quadruple<A, B, C, D> {
    return Quadruple(this.first, this.second, o.first, o.second)
}

infix fun <A, B, C, D> A.to(o: Triple<B, C, D>): Quadruple<A, B, C, D> {
    return Quadruple(this, o.first, o.second, o.third)
}

// Quadruple to Quintuple
infix fun <A, B, C, D, E> A.to(o: Quadruple<B, C, D, E>): Quintuple<A, B, C, D, E> {
    return Quintuple(this, o.first, o.second, o.third, o.fourth)
}

infix fun <A, B, C, D, E> Pair<A, B>.to(o: Triple<C, D, E>): Quintuple<A, B, C, D, E> {
    return Quintuple(this.first, this.second, o.first, o.second, o.third)
}

infix fun <A, B, C, D, E> Triple<A, B, C>.to(o: Pair<D, E>): Quintuple<A, B, C, D, E> {
    return Quintuple(this.first, this.second, this.third, o.first, o.second)
}

infix fun <A, B, C, D, E> Quadruple<A, B, C, D>.to(o: E): Quintuple<A, B, C, D, E> {
    return Quintuple(this.first, this.second, this.third, this.fourth, o)
}

infix fun <A, B, C, D, E, F> Quintuple<A, B, C, D, E>.to(o: F): Sextuple<A, B, C, D, E, F> {
    return Sextuple(first, second, third, fourth, fifth, o)
}

infix fun <A, B, C, D, E, F> Quadruple<A, B, C, D>.to(pair: Pair<E, F>): Sextuple<A, B, C, D, E, F> {
    return Sextuple(first, second, third, fourth, pair.first, pair.second)
}

infix fun <A, B, C, D, E, F> Triple<A, B, C>.to(triple: Triple<D, E, F>): Sextuple<A, B, C, D, E, F> {
    return Sextuple(first, second, third, triple.first, triple.second, triple.third)
}

infix fun <A, B, C, D, E, F> Pair<A, B>.to(quadruple: Quadruple<C, D, E, F>): Sextuple<A, B, C, D, E, F> {
    return Sextuple(first, second, quadruple.first, quadruple.second, quadruple.third, quadruple.fourth)
}

infix fun <A, B, C, D, E, F> A.to(quintuple: Quintuple<B, C, D, E, F>): Sextuple<A, B, C, D, E, F> {
    return Sextuple(this, quintuple.first, quintuple.second, quintuple.third, quintuple.fourth, quintuple.fifth)
}


infix fun <A, B, C, D, E, F, G> Sextuple<A, B, C, D, E, F>.to(o: G): Septuple<A, B, C, D, E, F, G> {
    return Septuple(first, second, third, fourth, fifth, sixth, o)
}

infix fun <A, B, C, D, E, F, G> Quintuple<A, B, C, D, E>.to(pair: Pair<F, G>): Septuple<A, B, C, D, E, F, G> {
    return Septuple(first, second, third, fourth, fifth, pair.first, pair.second)
}

infix fun <A, B, C, D, E, F, G> Quadruple<A, B, C, D>.to(triple: Triple<E, F, G>): Septuple<A, B, C, D, E, F, G> {
    return Septuple(first, second, third, fourth, triple.first, triple.second, triple.third)
}

infix fun <A, B, C, D, E, F, G> Triple<A, B, C>.to(quadruple: Quadruple<D, E, F, G>): Septuple<A, B, C, D, E, F, G> {
    val (d, e, f, g) = quadruple
    return Septuple(first, second, third, d, e, f, g)
}

infix fun <A, B, C, D, E, F, G> Pair<A, B>.to(quintuple: Quintuple<C, D, E, F, G>): Septuple<A, B, C, D, E, F, G> {
    val (c, d, e, f, g) = quintuple
    return Septuple(this.first, this.second, c, d, e, f, g)
}

infix fun <A, B, C, D, E, F, G> A.to(sextuple: Sextuple<B, C, D, E, F, G>): Septuple<A, B, C, D, E, F, G> {
    val (b, c, d, e, f, g) = sextuple
    return Septuple(this, b, c, d, e, f, g)
}
