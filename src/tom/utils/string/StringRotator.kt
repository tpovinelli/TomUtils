package tom.utils.string

import kotlin.math.abs

infix fun String.rotatedBy(offset: Int) = rotate(this, offset)

private const val FIRST_UPPER = 'A'
private const val FIRST_LOWER = 'a'
private val uppercase = charArrayOf('A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z')
private val lowercase = charArrayOf('a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z')

fun rotate(string: String, offset: Int): String {
  val output = StringBuilder()
  for (c in string) {
    when {
      Character.isUpperCase(c) -> output.append(uppercase[abs(c - FIRST_UPPER + offset) % 26])
      Character.isLowerCase(c) -> output.append(lowercase[abs(c - FIRST_LOWER + offset) % 26])
      else -> output.append(c)
    }
  }
  return output.toString()
}

private fun unrotateIndexFor(c: Char, first: Char, offset: Int): Int {
  val idx = c.toInt() - first.toInt() - offset
  return if (idx < 0) {
    26 - abs(idx)
  } else {
    idx % 26
  }
}

fun unrotate(string: String, offset: Int): String {
  val output = StringBuilder()
  for (c in string) {
    when {
      Character.isUpperCase(c) -> output.append(uppercase[unrotateIndexFor(c, FIRST_UPPER, offset)])
      Character.isLowerCase(c) -> output.append(lowercase[unrotateIndexFor(c, FIRST_LOWER, offset)])
      else -> output.append(c)
    }
  }
  return output.toString()
}

class StringRotator(val offset: Int) {

  fun rotate(string: String) = rotate(string, offset)

  fun unrotate(string: String) = unrotate(string, offset)

  companion object {

    // Example usage
    @JvmStatic
    fun main(args: Array<String>) {
      val rot13 = StringRotator(13)
      val rot6 = StringRotator(6)
      println(rot13.rotate("Hello")) // Uryyb
      println(rot13.rotate(rot13.rotate("Hello"))) // Hello
      println(rot13.unrotate(rot13.rotate("Hello"))) // Hello

      val rot7 = StringRotator(7)
      println(rot6.rotate("Hello")) // Nkrru
      println(rot7.rotate(rot6.rotate("Hello"))) // Uryyb
      println(rot6.unrotate(rot6.rotate("Hello"))) // Hello

      println("Thriller" rotatedBy 3)
      println("Hello" rotatedBy 13 rotatedBy 13)
    }
  }
}
