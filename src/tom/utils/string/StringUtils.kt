package tom.utils.string

import java.net.URI
import java.net.URL
import java.util.*

/**
 * @author Thomas Povinelli
 * Created 7/5/17
 * In TomUtils
 */
inline fun StringBuilder.appendFormat(
  format: String,
  vararg args: Any?
): StringBuilder {
    return append(format.format(*args))
}

inline fun StringBuilder.appendFormat(
  locale: Locale,
  format: String,
  vararg args: Any?
): StringBuilder {
    return append(format.format(locale, *args))
}

fun String.characters(): CharArray {
    val chars = CharArray(length)
    var i = 0
    for (char in this) {
        chars[i] = char
        i++
    }
    return chars
}

val String.size: Int get() = length

fun String.filterAlphaNumeric(): String {
    return this.replace("[^a-zA-Z0-9]+".toRegex(), "")
}


@Deprecated(
  "withoutPunctuation is not clear as some punctuation is not removed" +
    "specify the punctuation you want to remove with a Regex",
  ReplaceWith("this.replace(\"[put punctuation here]+\".toRegex(), \"\")")
)
fun String.withoutPunctuation(
  charset: CharSequence = ".,:;'\"?-+!/&*()"
): String {
    return replace("[${charset.escapedForCharacterClass()}]".toRegex(), "")
}

fun CharSequence.escapedForCharacterClass(): String {
    return this.replace("([\\-^])".toRegex()) { "\\${it.value}" }
}

fun String.tokens(delim: String = " "): List<String> {
    val t = StringTokenizer(this, delim)
    val l = mutableListOf<String>()
    while (t.hasMoreTokens()) {
        l.add(t.nextToken())
    }
    return l
}

operator fun String.times(other: Int): String {
    val s = StringBuilder(this.length * other)
    repeat(other) {
        s.append(this)
    }
    return s.toString()
}

operator fun Char.times(other: Int): String {
    val s = StringBuilder(other)
    repeat(other) {
        s.append(this)
    }
    return s.toString()
}

operator fun StringBuilder.plusAssign(obj: Any) {
    this.append(obj.toString())
}

operator fun <T> StringBuilder.plusAssign(arr: Array<T>) {
    this.append(arr.contentToString())
}

operator fun StringBuilder.plusAssign(str: String) {
    this.append(str)
}

operator fun StringBuilder.plusAssign(charSequence: CharSequence) {
    this.append(charSequence)
}

operator fun StringBuilder.plusAssign(stringBuilder: StringBuilder) {
    this.append(stringBuilder)
}

operator fun StringBuilder.plusAssign(charArray: CharArray) {
    this.append(charArray)
}

operator fun StringBuilder.plusAssign(i: Int) {
    this.append(i)
}

operator fun StringBuilder.plusAssign(bool: Boolean) {
    this.append(bool)
}

operator fun StringBuilder.plusAssign(lng: Long) {
    this.append(lng)
}

operator fun StringBuilder.plusAssign(byte: Byte) {
    this.append(byte)
}

operator fun StringBuilder.plusAssign(c: Char) {
    this.append(c)
}

operator fun StringBuilder.plusAssign(f: Float) {
    this.append(f)
}

operator fun StringBuilder.plusAssign(d: Double) {
    this.append(d)
}

fun String.toURL(): URL {
    return URI(this).toURL()
}

enum class StringSidePriority {
    LEFT, RIGHT
}

@JvmOverloads
fun String.lengthened(char: Char, length: Int, priority: StringSidePriority = StringSidePriority.RIGHT): String {
    val size = this.length
    if (size >= length) return this
    val needed = length - size
    val left = needed / 2
    val right = needed / 2
    var padded = this.padded(char, char, left, right)
    if (padded.length != length) {
        padded = when (priority) {
            StringSidePriority.LEFT  -> char + padded
            StringSidePriority.RIGHT -> padded + char
        }
    }
    return padded
}

fun String.padded(lChar: Char, rChar: Char, leftCount: Int, rightCount: Int): String {
    val result = StringBuilder()
    repeat(leftCount) { result.append(lChar) }
    result.append(this)
    repeat(rightCount) { result.append(rChar) }
    return result.toString()
}
