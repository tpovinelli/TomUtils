package tom.utils.delegates

import tom.utils.kotlin.trying
import kotlin.properties.ReadWriteProperty
import kotlin.reflect.KProperty

import tom.utils.math.min
import tom.utils.math.max

//internal fun <T: Comparable<T>> max(a: T, b: T): T = if (a > b) a else b
//internal fun <T: Comparable<T>> min(a: T, b: T): T = if (a < b) a else b

class BoundedValueException(msg: String): IllegalArgumentException(msg)

private fun <K> outOfBoundsMessage(value: Any?, boundsString: String, instance: K, property: KProperty<*>): String {
    val className = trying { instance!!::class.java.canonicalName } ?: "<unknown class>"
    return "Value $value for bounded property `${property.name}` in class " +
            "`$className` is out of bounds ($boundsString)"
}

private fun <K> throwOutOfBounds(value: Any?, boundsString: String, instance: K, property: KProperty<*>): Nothing {
    throw BoundedValueException(outOfBoundsMessage(value, boundsString, instance, property))
}

private fun <K> warnOnOutOfBounds(value: Any?, boundsString: String, instance: K, property: KProperty<*>) {
    println("WARNING: ${outOfBoundsMessage(value, boundsString, instance, property)}")
}

private fun <T> constructorBoundsCheck(
  initial: T,
  precondition: () -> Boolean,
  throwOnOOB: Boolean,
  emitWarning: Boolean,
  boundsString: String
) {
    if (!precondition()) {
        if (throwOnOOB) {
            throw BoundedValueException(
              "BoundedValue construction with initial value ($initial)" +
                "outside of bounds ($boundsString)"
            )
        } else if (emitWarning) {
            println(
              "WARNING: BoundedValue construction with initial " +
                "value ($initial) outside of bounds " +
                "($boundsString)"
            )
        }
    }
}

/**
 * Delegate for a property with a maximum value
 *
 * Example
 *
 * class Roster {
 *      var classSize: Int by TopBoundedValue(0, 35)
 *      // ...
 * }
 * // ...
 *
 * val r = Roster()
 * // ...
 * r.classSize // 35
 * r.classSize++ // 35
 *
 */
open class TopBoundedValue<in R, T: Comparable<T>>(var initial: T, val max: T, var emitWarning: Boolean = false, val throwOnOOB: Boolean = true): ReadWriteProperty<R, T> {
    init {
        val boundsString = "max: $max"
        constructorBoundsCheck(initial, { initial <= max }, throwOnOOB, emitWarning, boundsString)
        initial = min(initial, max)
    }


    override fun getValue(thisRef: R, property: KProperty<*>): T {
        return initial
    }

    override fun setValue(thisRef: R, property: KProperty<*>, value: T) {
        boundsCheck(value, thisRef, property)
        initial = min(value, max)
    }

    private fun boundsCheck(value: T, thisRef: R, property: KProperty<*>) {
        if (value > max) {
            if (throwOnOOB) {
                throwOutOfBounds(value, "max: $max", thisRef, property)
            } else if (emitWarning) {
                warnOnOutOfBounds(value, "max: $max", thisRef, property)
            }
        }
    }
}

/**
 * Delegate for a property with a minimum value
 *
 * Example
 *
 * class Sale {
 *      var daysRemaining: Int by BottomBoundedValue(7, 0)
 *      // ...
 * }
 * // ...
 *
 * val r = Sale()
 * // ...
 * r.daysRemaining // 1
 * r.daysRemaining-- // 0
 * r.daysRemaining-- // 0
 *
 */
open class BottomBoundedValue<in R, T: Comparable<T>>
(var initial: T, val min: T, var emitWarning: Boolean = false, val throwOnOOB: Boolean = true):
  ReadWriteProperty<R, T> {
    init {
        val boundsString = "min: $min"
        constructorBoundsCheck(initial, { initial >= min }, throwOnOOB, emitWarning, boundsString)
        initial = max(initial, min)
    }

    override fun getValue(thisRef: R, property: KProperty<*>): T {
        return initial
    }

    override fun setValue(thisRef: R, property: KProperty<*>, value: T) {
        boundsCheck(value, thisRef, property)
        initial = max(value, min)
    }

    private fun boundsCheck(value: T, thisRef: R, property: KProperty<*>) {
        if (value < min) {
            if (throwOnOOB) {
                throwOutOfBounds(value, "min: $min", thisRef, property)
            } else if (emitWarning) {
                warnOnOutOfBounds(value, "min: $min", thisRef, property)
            }
        }
    }
}

/**
 * Delegate for a property with both a maximum and minimum value
 *
 * See [TopBoundedValue] and [BottomBoundedValue]
 */
open class BoundedValue<in R, T: Comparable<T>>(
  var initial: T,
  val min: T,
  val max: T,
  val emitWarning: Boolean = false,
  val throwOnOOB: Boolean = true
):
  ReadWriteProperty<R, T> {

    companion object;

    init {
        val boundsString = "min: $min, max: $max"
        constructorBoundsCheck(initial, { initial in min..max }, throwOnOOB, emitWarning, boundsString)
        initial = min(max(initial, min), max)
    }

    override fun getValue(thisRef: R, property: KProperty<*>): T {
        return initial
    }

    override fun setValue(thisRef: R, property: KProperty<*>, value: T) {
        boundsCheck(value, thisRef, property)
        initial = min(max(value, min), max)
    }

    private fun boundsCheck(value: T, thisRef: R, property: KProperty<*>) {
        if (value < min || value > max) {
            if (throwOnOOB) {
                throwOutOfBounds(value, "min: $min, max: $max", thisRef, property)
            } else if (emitWarning) {
                warnOnOutOfBounds(value, "min: $min, max: $max", thisRef, property)
            }
        }
    }

}

interface Copyable<out T> {
    fun copy(): T
}

class Copying<in R, T>(var initial: T?): ReadWriteProperty<R, T?> where T : Copyable<T>{
    override fun getValue(thisRef: R, property: KProperty<*>): T? {
        return initial
    }

    override fun setValue(thisRef: R, property: KProperty<*>, value: T?) {
        initial = value?.copy()
    }

}

class Rounded<in R>(var initial: Double, val toPlaces: Int): ReadWriteProperty<R, Double> {
    override fun setValue(thisRef: R, property: KProperty<*>, value: Double) {
        initial = (value * forPlaces()).toInt().toDouble() / forPlaces()
    }

    private fun forPlaces(): Int {
        var x = 1
        for (i in 0 until toPlaces) {
            x *= 10
        }
        return x
    }

    override fun getValue(thisRef: R, property: KProperty<*>): Double {
        return initial
    }

}