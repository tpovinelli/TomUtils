package tom.utils.math

import tom.utils.iter.frequencies
import kotlin.math.ceil
import kotlin.math.floor

/**
 * @author Thomas Povinelli
 * Created 2017-Sep-19
 * In TomUtils
 */

val Int.isEven: Boolean get() = this % 2 == 0
val Int.isOdd: Boolean get() = !(this.isEven)
infix fun Int.isMultipleOf(other: Int) = this % other == 0

val Long.isEven: Boolean get() = this % 2L == 0L
val Long.isOdd: Boolean get() = !(this.isEven)
infix fun Long.isMultipleOf(other: Long) = this % other == 0L

val Short.isEven: Boolean get() = this % 2 == 0
val Short.isOdd: Boolean get() = !(this.isEven)
infix fun Short.isMultipleOf(other: Short) = this % other == 0

infix fun Int.atLeast(floor: Int) =
  if (this < floor) floor else this

infix fun Int.noMoreThan(ceiling: Int) =
  if (this > ceiling) ceiling else this

infix fun Long.atLeast(floor: Long) =
  if (this < floor) floor else this

infix fun Long.noMoreThan(ceiling: Long) =
  if (this > ceiling) ceiling else this

infix fun Double.atLeast(floor: Double) =
  if (this < floor) floor else this

infix fun Double.noMoreThan(ceiling: Double) =
  if (this > ceiling) ceiling else this



inline fun <reified T : Comparable<T>> max(l: List<T>): T {
    return max(*l.toTypedArray())
}

inline fun <reified T : Comparable<T>> min(l: List<T>): T {
    return min(*l.toTypedArray())
}

fun <T : Comparable<T>> max(vararg args: T): T {
    var maximum: T = args.first()
    for (element in args) {
        if (element > maximum) {
            maximum = element
        }
    }
    return maximum
}

fun <T : Comparable<T>> min(vararg args: T): T {
    var minimum: T = args.first()
    for (element in args) {
        if (element < minimum) {
            minimum = element
        }
    }
    return minimum
}

fun random(range: IntRange): Long {
    return random(range.first.toLong(), range.last.toLong())
}

fun random(start: Long, endInclusive: Long): Long {
    return (Math.random() * (endInclusive - start) + start).toLong()
}

//fun random(range: LongRange): Long {
//    return random(range.start, range.endInclusive)
//}

fun <T> Array<T>.randomItem(): T {
    return this[(Math.random() * size).toInt()]
}

fun <T> Collection<T>.randomItem(): T {
    return this.elementAt((Math.random() * size).toInt())
}

fun <T> random(array: Array<T>): T {
    val length = array.size
    val index = (Math.random() * length).toInt()
    return array[index]
}

fun <T> random(list: List<T>): T {
    val length = list.size
    val index = (Math.random() * length).toInt()
    return list[index]
}

fun random(low: Double, high: Double): Double {
    return Math.random() * (high - low) + low
}

fun random(): Double {
    return Math.random()
}


val <T: Comparable<T>> Iterable<T>.mode: Collection<T>
    get() {
        var ret = mutableListOf<T>()
        val counts = this.frequencies()
        var max = -1
        for ((elt, count) in counts) {
            if (count > max) {
                max = count
                ret = mutableListOf(elt)
            } else if (count == max) {
                ret.add(elt)
            }
        }
        return ret
    }


sealed class Median<out T> {
    fun <R> fold(simple: (T) -> R, tied: (T, T) -> R): R {
        return when (this) {
            is SimpleMedian<T> -> simple(median)
            is TiedMedian<T>   -> tied(lower, higher)
        }
    }

    fun <R> each(simple: (SimpleMedian<T>) -> R, tied: (TiedMedian<T>) -> R): R {
        return when (this) {
            is SimpleMedian<T> -> simple(this)
            is TiedMedian<T>   -> tied(this)
        }
    }
}

data class SimpleMedian<out T>(val median: T): Median<T>()
data class TiedMedian<out T>(val lower: T, val higher: T): Median<T>()

@JvmName("collapseInt")
fun TiedMedian<Int>.collapse() = (lower + higher) / 2.0

@JvmName("collapseDouble")
fun TiedMedian<Double>.collapse() = (lower + higher) / 2.0

fun <T> TiedMedian<T>.collapse(transform: (T, T) -> T) = transform(lower, higher)

val <T: Comparable<T>> List<T>.median: Median<T>
    get() {
        val l: List<T> = this.sorted()
        val middle = l.size / 2.0
        return if (middle - middle.toInt() != 0.0) {
            SimpleMedian(l[middle.toInt()])
        } else {
            TiedMedian(l[floor(middle).toInt()], l[ceil(middle).toInt()])
        }
    }
