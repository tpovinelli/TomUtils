package tom.utils.math

import kotlin.math.sqrt


class KComplex(val real: Double, val imag: Double) {

    val magnitude: Double
        get() = sqrt(real.squared() + imag.squared())

    override fun toString(): String {
        return "" + real + (if (imag > 0) "+" else "-") + Math.abs(imag) + "j"
    }

    operator fun plus(real: Double): KComplex {
        return KComplex(this.real + real, imag)
    }

    operator fun plus(complex: KComplex): KComplex {
        return KComplex(this.real + complex.real, this.imag + complex.imag)
    }

    operator fun times(real: Double): KComplex {
        return KComplex(this.real * real, this.imag * real)
    }

    operator fun times(complex: KComplex): KComplex {
        val first = this.real * complex.real // real
        val outer = this.real * complex.imag // imag
        val inner = this.imag * complex.real // imag
        var last = this.imag * complex.imag // real
        last = -last // i*i = -1
        return KComplex(first + last, outer + inner)
    }

    operator fun minus(complex: KComplex): KComplex {
        return KComplex(this.real - complex.real, this.imag - complex.imag)
    }

    operator fun minus(real: Double): KComplex {
        return KComplex(this.real - real, this.imag)
    }

    operator fun unaryMinus(): KComplex {
        return KComplex(-this.real, -this.imag)
    }

    fun negate(): KComplex = -this

    fun conjugate(): KComplex = !this

    operator fun not(): KComplex {
        return KComplex(this.real, -this.imag)
    }

    operator fun div(real: Double): KComplex {
        return KComplex(this.real / real, this.imag / real)
    }

    operator fun div(complex: KComplex): KComplex {
        val conjugate = complex.conjugate()
        val numerator = this * conjugate
        val denominator = complex * conjugate
        assert(denominator.imag == 0.0)
        return numerator.div(denominator.real)
    }

    fun squared(): KComplex {
        return this * this
    }

    fun hasRealComponent(): Boolean {
        return real != 0.0
    }

    fun hasImaginaryComponent(): Boolean {
        return imag != 0.0
    }

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as KComplex

        if (real != other.real) return false
        if (imag != other.imag) return false

        return true
    }

    override fun hashCode(): Int {
        var result = real.hashCode()
        result = 31 * result + imag.hashCode()
        return result
    }

    operator fun component1(): Double = real
    operator fun component2(): Double = imag

    companion object {
        val ZERO = KComplex(0.0, 0.0)
        val ONE = KComplex(1.0, 0.0)
        val I = KComplex(0.0, 1.0)
        val NEGATIVE_ONE = KComplex(-1.0, 0.0)
        val NEGATIVE_I = KComplex(0.0, -1.0)
        val ROOT_2 = KComplex(1.0, 1.0)
        val NEGATIVE_ROOT_2 = KComplex(-1.0, -1.0)
    }
}

val Double.j: KComplex get() = KComplex(0.0, this)
fun Double.squared() = this * this

operator fun Double.plus(c: KComplex): KComplex {
    return c + this
}

operator fun Double.minus(c: KComplex): KComplex {
    return (-c) + (this)
}

val Complex.k: KComplex get() = KComplex(this.realComponent(), this.imaginaryComponent())

val KComplex.java: Complex get() = Complex(real, imag)

fun main() {

    val c = KComplex(3.0, 4.0)
    val d = !(c * 2.0)
    val e = -(d / KComplex(5.0, -1.0))
    val f = 3.0 + 4.0.j

    println(listOf(c, d, e, f))
    println(c == f)
    println(c.magnitude > 0)

}