package tom.utils.math

import kotlin.math.pow
import kotlin.math.sqrt


/**
 * @author Thomas Povinelli
 * Created 5/24/18
 * In TomUtils
 */

data class Point(val x: Double, val y: Double) {

  constructor(initValues: Pair<Double, Double>) : this(initValues.first, initValues.second)

  constructor(copy: Point) : this(copy.x, copy.y)

  companion object {
    @JvmStatic
    val origin: Point = Point(0.0, 0.0)

    @JvmStatic
    fun midpointBetween(
      startX: Double, startY: Double, endX: Double, endY: Double
    ) = Point((startX + endX) / 2, (startY + endY) / 2)

    @JvmStatic
    fun distanceBetween(
      startX: Double, startY: Double, endX: Double, endY: Double
    ) = sqrt((startX - endX).pow(2.0) + (startY - endY).pow(2.0))
  }

  fun distanceToOrigin() = distanceTo(origin)
  fun distanceTo(other: Point): Double {
    return sqrt((this.x - other.x).pow(2.0) + (this.y - other.y).pow(2.0))
  }

  fun midpointTo(other: Point): Point {
    return Point((this.x + other.x) / 2, (this.y + other.y) / 2)
  }

}
