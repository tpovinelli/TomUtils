package tom.utils.math;

import org.jetbrains.annotations.NotNull;

public class Complex {

    public static final Complex NEGATIVE_ONE = new Complex(-1, 0);
    public static final Complex ONE = new Complex(1, 0);
    public static final Complex ZERO = new Complex(0, 0);
    public static final Complex I = new Complex(0, 1);
    public static final Complex NEGATIVE_I = new Complex(0, -1);
    public static final Complex ROOT_2 = new Complex(1.0, 1.0);
    public static final Complex NEGATIVE_ROOT_2 = new Complex(-1.0, -1.0);

    private final double real;
    private final double imag;

    public Complex(double real, double imag) {
        this.real = real;
        this.imag = imag;
    }

    public String toString() {
        return "" + real + (imag > 0 ? "+" : "-") + Math.abs(imag) + "j";
    }

    @NotNull
    public Complex add(double real) {
        return new Complex(this.real + real, imag);
    }

    @NotNull
    public Complex add(@NotNull Complex complex) {
        return new Complex(this.real + complex.real, this.imag + complex.imag);
    }

    @NotNull
    public Complex mul(double real) {
        return new Complex(this.real * real, this.imag * real);
    }

    @NotNull
    public Complex mul(@NotNull Complex complex) {
        double first = this.real * complex.real; // real
        double outer = this.real * complex.imag; // imag
        double inner = this.imag * complex.real; // imag
        double last = this.imag * complex.imag; // real
        last = -last; // i*i = -1
        return new Complex(first + last, outer + inner);
    }

    @NotNull
    public Complex sub(@NotNull Complex complex) {
        return new Complex(this.real - complex.real, this.imag - complex.imag);
    }

    @NotNull
    public Complex sub(double real) {
        return new Complex(this.real - real, this.imag);
    }

    @NotNull
    public Complex negate() {
        return new Complex(-this.real, -this.imag);
    }

    @NotNull
    public Complex conjugate() {
        return new Complex(this.real, -this.imag);
    }

    @NotNull
    public Complex div(double real) {
        return new Complex(this.real / real, this.imag / real);
    }

    @NotNull
    public Complex div(@NotNull Complex complex) {
        Complex conjugate = complex.conjugate();
        Complex numerator = this.mul(conjugate);
        Complex denominator = complex.mul(conjugate);
        assert denominator.imag == 0;
        return numerator.div(denominator.real);
    }

    @NotNull
    public Complex squared() {
        return this.mul(this);
    }

    public double realComponent() {
        return real;
    }

    public double imaginaryComponent() {
        return imag;
    }

    public boolean hasRealComponent() {
        return real != 0;
    }

    public boolean hasImaginaryComponent() {
        return imag != 0;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Complex complex = (Complex) o;

        if (Double.compare(complex.real, real) != 0) return false;
        return Double.compare(complex.imag, imag) == 0;

    }

    @Override
    public int hashCode() {
        int result;
        result = Double.hashCode(real);
        result = 31 * result + Double.hashCode(imag);
        return result;
    }

    public double magnitude() {
        return Math.sqrt(real * real + imag * imag);
    }
}
