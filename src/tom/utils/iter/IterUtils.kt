package tom.utils.iter

import tom.utils.algebraic.Left
import tom.utils.algebraic.Result
import tom.utils.algebraic.Right
import tom.utils.kotlin.guard
import java.io.Serializable
import kotlin.math.roundToInt

// Reports the difference between the supplied end (inclusive) and start of the range
// for a range such as 0..10 step 6 this would return 10 since the range is originally
// on 0 to 10. The range 4..30 step 17 would return 26 since the original range is 26 wide
val IntRange.width: Int get() = endInclusive - start
val LongRange.width: Long get() = endInclusive - start
val CharRange.width: Int get() = endInclusive - start

// Returns the actual bitsSize of the range considering the step of the range
// for instance the range 0..10 step 6 would return 6 since the last value is 6
// the range 4..30 step 17 would return 17 since the last element is 21 and the
// start is 4. 1..12 step 2 would give 10 since the last one would be 11 and the start is 1
val IntProgression.size: Int get() = last - first
val LongProgression.size: Long get() = last - first
val CharProgression.size: Int get() = last - first

// Returns the actual number of elements in the range considering the step
// for 0..10 step 6 it would return 2 since there is the first step at 0
// the next at 6 and then the range is over
// for 4..30 step 17 it would give 2 as well because the first is 4 the next is
// 21 and then the range is over
// 1..12 step 2 would return 6 for 1, 3, 5, 7, 9, 11 and then the range ends
// These work exactly the same as [Iterable<T>.count()] except these are not
// timed on O(n) these are constant time operations
val IntProgression.count: Int get() = (last - first) / step + 1
val LongProgression.count: Long get() = (last - first) / step + 1
val CharProgression.count: Int get() = (last - first) / step + 1

@Deprecated("Use built-in `indices` property instead", ReplaceWith("indices"))
val <T> Collection<T>.range: IntRange get() = indices

@JvmInline
value class Percentage(val value: Double) {
    init {
        require(value in (0.0..100.0)) { "Percentage should be between 0 and 100" }
    }

    val rate: Double get() = value / 100.0
}

val Int.percent: Percentage get() = Percentage(this.toDouble())

enum class IndexMethod {
    NotExceeding,
    NotLessThan,
    ClosestIndex,
}

fun <T> Collection<T>.indexAtPercentage(percentage: Percentage, method: IndexMethod = IndexMethod.ClosestIndex): Int {
    val fractional = (this.size * percentage.rate)
    return when (method) {
        IndexMethod.NotExceeding -> fractional.toInt()
        IndexMethod.NotLessThan -> if (fractional.toInt().toDouble() == fractional) fractional.toInt() else (fractional + 1).toInt()
        IndexMethod.ClosestIndex -> fractional.roundToInt()
    }
}

inline fun <E> Iterable<E>.peek(action: (elt: E) -> Unit): Iterable<E> {
    for (element in this) {
        action(element)
    }
    return this
}

/**
 * Used to indicate which list in a collection of partitions an element should
 * be added to see [partition]
 */
enum class Index {
    ZERO, ONE, TWO, THREE, FOUR, FIVE, SIX, SEVEN, EIGHT, NINE, TEN,
    ELEVEN, TWELVE, THIRTEEN, FOURTEEN, FIFTEEN, SIXTEEN, SEVENTEEN,
    EIGHTEEN, NINETEEN, TWENTY;

    fun toInt(): Int {
        return this.ordinal
    }
}

/**
 * Partition the list into [into] separate lists based on the return value of
 * [indicator]. The [indicator] must not return any [Index] values whose
 * [Enum.ordinal] value is greater than or equal to the number of lists
 * as provided by [indicator]. Further, [indicator]'s return values are used
 * as indices into the list of lists that is returned to determine which
 * of the lists to add the particular element to. [indicator] ought to always
 * return values starting at [Index.ZERO] and being contiguous
 */
fun <T> Iterable<T>.partition(into: Int, indicator: (T) -> Index): List<List<T>> {
    val x = List(into) { mutableListOf<T>() }
    for (element in this) {
        val t = indicator(element)
        if (t.toInt() >= into) {
            throw IllegalArgumentException("Index $t (${t.toInt()}) is out of bounds for partitioning into $into groups")
        }
        x[t.toInt()].add(element)
    }
    return x
}


/**
 * Transforms both elements of a Pair into a new Pair with different types
 */
fun <K, L : K, R> Pair<K, L>.map(transform: (K) -> R): Pair<R, R> {
    return transform(first) to transform(second)
}

@Deprecated("duplicate functionality", ReplaceWith("this.withIndex().peek(action)"), DeprecationLevel.WARNING)
inline fun <T, C : Iterable<T>> C.onEachIndexed(action: (idx: Int, elt: T) -> Unit): C {
    return apply { for (element in this.withIndex()) action(element.index, element.value) }
}

@Deprecated("duplicate functionality", ReplaceWith("this.withIndex().peek(action)"), DeprecationLevel.WARNING)
inline fun <E> Iterable<E>.passThroughIndexed(action: (idx: Int, elt: E) -> Unit): Iterable<E> {
    for (element in this.withIndex()) {
        action(element.index, element.value)
    }
    return this
}

inline infix fun IntRange.each(f: (Int) -> Unit) {
    for (i in start until endInclusive) {
        f(i)
    }
}

inline infix fun Int.times(f: (i: Int) -> Unit): Int {
    for (i in 0 until this) {
        f(i)
    }
    return this
}

inline fun <R> Int.times(f: (i: Int) -> R): List<R> {
    val result = mutableListOf<R>()
    for (i in 0 until this) {
        result.add(f(i))
    }
    return result
}

@Deprecated("Passing args to Int.times is deprecated. Use closure instead", level = DeprecationLevel.ERROR)
inline fun <T> Int.times(f: (args: Array<out T>) -> Unit, vararg args: T): Int {
    for (i in 0 until this) {
        f(args)
    }
    return this
}

fun <E> Iterable<E>.frequencies(): Map<E, Int> {
    val map = HashMap<E, Int>()
    for (element in this) {
        map[element] = (map[element] ?: 0) + 1
    }
    return map
}

@Deprecated(
    "Following conventions of the standard library, invert() has been renamed to inverted()",
    ReplaceWith("this.inverted()")
)
fun <K, V> Map<K, V>.invert(): Map<V, List<K>> = this.inverted()

fun <K, V> Map<K, V>.inverted(): Map<V, List<K>> {
    val result = HashMap<V, MutableList<K>>()
    for ((key, value) in this.entries) {
        result.compute(value) { _, oldValue ->
            val newList = oldValue ?: mutableListOf()
            newList.add(key)
            newList
        }
    }
    return result
}

/**
 * This method takes a map of K to V and first transforms it to a map of
 * V to List<K>. This is because keys of a map must be unique but values
 * do not have to be unique. Therefore it is possible to have multiple keys
 * with the same value in the original map, so they are combined under the
 * value in the resulting map.
 *
 * After creating this map the function then updates every entry in the map
 * by passing the List<K> through the [keyCombiner] function which produces
 * some R. This value is then used as a the value for the key of type V
 * in the map that is returned
 *
 */
fun <K, V, R> Map<K, V>.inverted(keyCombiner: (List<K>) -> R): Map<V, R> {
    val inverted = this.inverted()
    val retval = mutableMapOf<V, R>()
    for ((k, listv) in inverted) {
        retval[k] = keyCombiner(listv)
    }
    return retval
}

fun <T> Iterable<T>.uniqueElements(): Iterable<T> {
    val ret = mutableSetOf<T>()
    for (element in this) {
        ret.add(element)
    }
    return ret
}

fun <T> List<T>.allIndicesOf(elt: T?): List<Int> {
    val indices = mutableListOf<Int>()
    for ((index, element) in this.withIndex()) {
        if (elt == element) {
            indices.add(index)
        }
    }
    return indices
}

internal fun mapCapacity(expectedSize: Int): Int {
    if (expectedSize < 3) {
        return expectedSize + 1
    }
    if (expectedSize < Int.MAX_VALUE / 2 + 1) {
        return expectedSize + expectedSize / 3
    }
    return Int.MAX_VALUE // any large value
}

internal fun <K, V, M : MutableMap<K, V>> M.emptyCheck(): Map<K, V> {
    return when (size) {
        0 -> emptyMap<K, V>()
        1 -> java.util.Collections.singletonMap(keys.first(), values.first())
        else -> this
    }
}

fun <K, V> Iterable<Pair<K, V>>.toUnorderedMap(): Map<K, V> {
    if (this is Collection) {
        return when (size) {
            0 -> emptyMap()
            1 -> mapOf(if (this is List) this[0] else iterator().next())
            else -> toMap(HashMap(mapCapacity(size)))
        }
    }
    return toMap(HashMap()).emptyCheck()
}

fun <K, V> Iterable<Pair<K, V>>.toMutableMap(): MutableMap<K, V> {
    val hashMap = LinkedHashMap<K, V>()
    forEach { hashMap[it.first] = it.second }
    return hashMap
}

fun <K, V> Iterable<Pair<K, V>>.toMutableUnorderedMap(): MutableMap<K, V> {
    val hashMap = HashMap<K, V>()
    forEach { hashMap[it.first] = it.second }
    return hashMap
}


data class Bounds<T : Comparable<T>>(val min: T, val max: T)

val <T : Comparable<T>> Iterable<T>.bounds: Bounds<T>?
    get() {
        val iterator = iterator()
        if (!iterator.hasNext()) return null
        var min = iterator.next()
        var max = min
        for (element in iterator) {
            if (element < min)
                min = element

            if (element > max)
                max = element
        }
        return Bounds(min, max)
    }

class MinMaxAverage<T>(var min: T, var max: T) where T : Number, T : Comparable<T> {
    internal var total: Double = 0.0
    internal var count: Int = 0

    override fun toString(): String = "MinMaxAverage(min=$min, max=$max, average=$average)"

    val sum: Double get() = total
    val average: Double get() = total / count

    fun accumulator(other: T): MinMaxAverage<T> {
        min = if (other < min) other else min
        max = if (other > max) other else max
        count += 1
        total += other.toDouble()
        return this
    }
}

fun <T> Iterable<T>.reduceMinMaxAverage(): MinMaxAverage<T> where T : Number, T : Comparable<T> {
    val i = iterator()
    guard(i.hasNext()) { throw UnsupportedOperationException("Cannot reduce and empty Iterable") }
    val first = i.next()
    return fold(MinMaxAverage(first, first), MinMaxAverage<T>::accumulator)
}

fun <T> Iterator<T>.reduceMinMaxAverage(): MinMaxAverage<T> where T : Number, T : Comparable<T> {
    guard(hasNext()) { throw UnsupportedOperationException("Cannot reduce and empty Iterable") }
    val first = next()
    var min: T = first
    var max: T = first
    var total: Double = 0.0
    var count: Int = 0
    for (i in this) {
        min = if (i < min) i else min
        max = if (i > max) i else max
        count++
        total += i.toDouble()

    }
    return MinMaxAverage(min, max).apply { this.total = total; this.count = count }

}

object NotFound : Exception() {
    private fun readResolve(): Any = NotFound
}

object EndOfList : Exception() {
    private fun readResolve(): Any = EndOfList
}

interface AIterator<T> {
    fun next(): Result<T, EndOfList>
}

fun <T> Iterator<T>.asAIterator(): AIterator<T> = object : AIterator<T> {
    override fun next(): Result<T, EndOfList> {
        return if (hasNext()) Left(this@asAIterator.next()) else Right(EndOfList)
    }
}

fun <T> Iterator<T>.firstMatching(predicate: (T) -> Boolean): Result<T, NotFound> {
    var result: Result<T, NotFound> = Right(NotFound)
    for (elt in this) { if (predicate(elt)) { result = Left(elt); break } }
    return result
}

fun <T> Iterator<T>.lastMatching(predicate: (T) -> Boolean): Result<T, NotFound> {
    var value: Result<T, NotFound> = Right(NotFound)
    for (elt in this) if (predicate(elt)) value = Left(elt)
    return value
}
