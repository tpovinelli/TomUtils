package tom.utils.iter.mutable

/**
 * @author Thomas Povinelli
 * Created 6/11/18
 * In TomUtils
 */

fun <T> MutableList<T>.reverseInPlace() {
    var low = 0
    var high = this.size - 1
    while (low < high) {
        val temp = this[low]
        this[low] = this[high]
        this[high] = temp
        low++
        high--
    }
}

@Deprecated("MutableList<T>.swap() could cause performance problems. swap() should not be called on Collections that do not implement RandomAccess")
fun <T> MutableList<T>.swap(elementAt: Int, withElementAt: Int) {
    val temp = this[elementAt]
    this[elementAt] = this[withElementAt]
    this[withElementAt] = temp
}

fun <E, C> C.swap(elementAt: Int, withElementAt: Int)
  where C: RandomAccess, C: MutableList<E> {
    val temp = this[elementAt]
    this[elementAt] = this[withElementAt]
    this[withElementAt] = temp
}

@Deprecated("MutableList<T>.shuffle() could cause performance problems. shuffle() should not be called on Collections that do not implement RandomAccess")
fun <E> MutableList<E>.shuffle() {
    for ((index, _) in this.iterator().withIndex()) {
        val randomEnd: Int = (Math.random() * this.size).toInt()
        swap(index, randomEnd)
    }
}

fun <E, C> C.shuffle()
  where C: RandomAccess, C: MutableList<E> {
    for (index in this.indices) {
        val randomEnd: Int = (Math.random() * this.size).toInt()
        swap(index, randomEnd)
    }
}


/**
 * Returns whatever was replaced
 */
fun <T> MutableList<T>.replaceAt(at: Int, withElement: T): T {
    val oldElement = this.removeAt(index = at)
    this.add(at, withElement)
    return oldElement
}


/**
 * returns true if an element was replaced otherwise false
 */
fun <T> MutableList<T>.replace(obj: T, withElement: T): Boolean {
    val tempIndex = this.indexOf(obj)
    return if (tempIndex != -1) {
        val old = this.remove(obj)
        if (old) {
            this.add(tempIndex, withElement)
            true
        } else {
            false
        }
    } else {
        false
    }
}

/**
 * Returns number of replacements made
 */
fun <T> MutableList<T>.replaceAll(obj: T, withElement: T): Int {
    var result: Boolean
    var count = 0
    for ((idx, element) in this.withIndex()) {
        if (element == obj) {
            result = this.remove(obj)
            if (result) {
                this.add(idx, withElement)
                count++
            }
        }
    }
    return count
}
