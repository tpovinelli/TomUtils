package tom.utils.iter

class BitIterator(
    val value: Int,
    val bitOrder: BitOrder = BitOrder.LSB_FIRST,
    val preservePosition: Boolean = false,
    val skipZeroes: Boolean = false
) : Iterator<Int> {

    private val positionOffset = if (bitOrder == BitOrder.LSB_FIRST) 1 else -1

    private var currentPos: Int = if (bitOrder == BitOrder.LSB_FIRST) 0 else 31

    private fun advancePosition() {
        currentPos += positionOffset
    }

    enum class BitOrder {
        MSB_FIRST, LSB_FIRST
    }

    override fun hasNext(): Boolean {
        return if (bitOrder == BitOrder.LSB_FIRST) {
            currentPos < 31
        } else {
            currentPos > 0
        }
    }

    override fun next(): Int {
        while (skipZeroes && (value and currentPos) == 0 && currentPos in 0..31) {
            advancePosition()
        }

        val result = value and currentPos

        if (preservePosition) {
            result shl currentPos
        }

        advancePosition()
        return result
    }
}
