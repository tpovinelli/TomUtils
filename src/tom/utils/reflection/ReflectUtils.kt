package tom.utils.reflection

/**
 * @author Thomas Povinelli
 * Created 9/29/19
 * In TomUtils
 */
/* skip 2 frames.
    * stack[0] is getStackTrace() and stack[1] is checkCallerClass()
    * methods with $ such as access$000 do not belong to the caller class
    * these methods sometimes intervene between the current frame and the caller class
    */
val callerStackFrame: StackTraceElement?
    get() =
        Thread.currentThread().stackTrace.drop(2).firstOrNull { !it.methodName.contains("$") }


enum class CheckCallerMethod {
    specifyAllowed, specifyDenied
}

@Throws(IllegalAccessException::class)
@JvmOverloads
fun checkCallerClass(
    caller: Class<*>,
    msg: String,
    method: CheckCallerMethod = CheckCallerMethod.specifyAllowed
) =
    checkCallerClass(arrayOf(caller), msg, method)

@Throws(IllegalAccessException::class)
@JvmOverloads
fun checkCallerClass(
    callers: Array<Class<*>>,
    msg: String,
    method: CheckCallerMethod = CheckCallerMethod.specifyAllowed
) {

    callerStackFrame?.let {
        val callerClassName = it.className
        println("caller class $callerClassName")
        for (clazz in callers) {
            if (clazz.name.equals(callerClassName, ignoreCase = true)) {
                if (method === CheckCallerMethod.specifyAllowed)
                    return@checkCallerClass
                else
                    throw IllegalAccessException(msg)
            }
        }
    }

    if (method === CheckCallerMethod.specifyAllowed)
        throw IllegalAccessException(msg)
}
