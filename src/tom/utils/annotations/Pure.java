package tom.utils.annotations;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * @author Thomas Povinelli
 * Created 6/24/18
 * In TomUtils
 *
 * Indicates the specified parameter is not mutated by the method in any way.
 * Generally you should annotate parameters that get mutated as {@link Inout}
 * and leave other parameters alone or mark them as final
 * but this annotation exists for convenience
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.PARAMETER)
public @interface Pure {
}
