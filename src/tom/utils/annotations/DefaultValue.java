package tom.utils.annotations;

/**
 * @author Thomas Povinelli
 * Created 6/24/18
 * In TomUtils
 */
public enum DefaultValue {
  EMPTY_OBJECT, // default value should be new Object()
  EMPTY_SAME_TYPE, // default value should be a new instance of the corresponding type, eg new MyTypeClass()
  NULL, // default value should be null
  MINUS_ONE, // default value is -1
  ZERO, // default value is 0
  MAX_VALUE, // default value is {Some Numeric Type}.MAX_VALUE (eg Integer.MAX_VALUE)
  MIN_VALUE, // default value is {Some Numeric Type}.MIN_VALUE (eg Integer.MIN_VALUE)
}
