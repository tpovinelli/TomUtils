package tom.utils.annotations

/**
 * @author Thomas Povinelli
 * Created 2019-Jan-20
 * In TomUtils
 *
 * Indicates a parameter should always be passed in Kotlin by using
 * the name as a keyword argument
 */
@Retention(AnnotationRetention.RUNTIME)
@Target(AnnotationTarget.VALUE_PARAMETER)
annotation class PassWithName
