package tom.utils.annotations;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * @author Thomas Povinelli
 * Created 6/24/18
 * In TomUtils
 *
 * Indicates that the specified parameter is mutated by the method. Generally,
 * this should be used along with no annotation when a parameter is not mutated
 * however for convenience a {@link Pure} annotation exists as well
 *
 */
@Target(ElementType.PARAMETER)
@Retention(RetentionPolicy.RUNTIME)
public @interface Inout {

}
