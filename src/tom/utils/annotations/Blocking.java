package tom.utils.annotations;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * @author Thomas Povinelli
 * Created 2018-Jan-02
 * In TomUtils
 * <p>
 * Indicates the method being called will block the calling thread if necessary
 * value indicates if the blocking can be or must be quantified by a time out
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.METHOD)
public @interface Blocking {
    TimeoutPolicy value();
}

