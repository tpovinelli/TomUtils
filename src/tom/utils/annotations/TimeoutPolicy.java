package tom.utils.annotations;

/**
 * @author Thomas Povinelli
 * Created 2018-Jan-02
 * In TomUtils
 * <p>
 * Indicates that a method using the {@link Blocking} annotation follows
 * a certain TimoutPolicy according to the follwing
 * <p>
 * NO_TIMEOUT : blocking cannot be timed out
 * CAN_TIMEOUT : blocking can be timed out but doesn't have to be
 * NEEDS_TIMEOUT : blocking must be qualified by timeout; blocking may stop or time out
 * ALWAYS_TIMEOUT : blocking can only be ended by some timeout
 */
public enum TimeoutPolicy {

  // blocking cannot be timed out
  NO_TIMEOUT,

  // blocking can be timed out, either block indefinitely or specify a period before timeout
  CAN_TIMEOUT,

  // blocking must be qualified by timeout; blocking may stop on its own or will stop when timed out. User must supply a timeout time
  NEEDS_TIMEOUT,

  // blocking can only be ended by some timeout expiring
  ALWAYS_TIMEOUT
}
