package tom.utils.annotations

/**
 * @author Thomas Povinelli
 * Created 10/2/19
 * In TomUtils
 */

/**
 * Indicates that the value passed should be constructed where it is passed
 * rather than a being passed from a variable/field/external resource
 *
 * If a class must be the only holder of a resource, it is best to
 * construct that resource in the constructor of the class that holds it
 * to avoid leaking the resource
 */
annotation class ConstructInPlace
