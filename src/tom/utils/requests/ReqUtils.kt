package tom.utils.requests

import tom.utils.algebraic.Left
import tom.utils.algebraic.Right
import tom.utils.iter.times
import java.io.File
import java.io.IOException
import java.net.URL
import java.nio.ByteBuffer
import java.nio.channels.Channels
import java.nio.channels.ReadableByteChannel


/**
 * @author Thomas Povinelli
 * Created 6/1/17
 * In TomUtils
 */

@Throws(IOException::class)
fun URL.fetchBytes(): ByteArray? {
    val rbc: ReadableByteChannel = try {
        Channels.newChannel(this.openStream())
    } catch (e: IOException) {
        return null
    }

    val bytes = ArrayList<Byte>()
    val buffer = ByteBuffer.allocate(0x4000)
    var c: Int

    do {
        c = rbc.read(buffer)
        val t = buffer.position()
        buffer.rewind()
        repeat(t) {
            bytes.add(buffer.get())
        }
        buffer.rewind()
        buffer.clear()
    } while (c > -1)

    return bytes.apply { trimToSize() }.toByteArray()
}

@Deprecated(
  message = "sourceCode is a misleading name and it shouldn't return a String.",
  replaceWith = ReplaceWith(
    "this.fetchBytes()?.toString(Charset.defaultCharset())",
    "java.nio.charset.Charset"
  )
)
fun URL.sourceCode(): String? {

    val rbc: ReadableByteChannel
    try {
        rbc = Channels.newChannel(this.openStream())
    } catch (e: IOException) {
        println("Error: ${e.message}")
        return null
    }

    val str = StringBuffer()
    val buffer = ByteBuffer.allocate(0x4000)
    var c: Int

    do {
        c = rbc.read(buffer)
        val t = buffer.position()
        buffer.rewind()
        t.times {
            str.append(buffer.get().toChar())
        }
        buffer.rewind()
        buffer.clear()
    } while (c > -1)

    return str.toString()
}

fun downloadFileAtUrl(url: URL, location: File): File? {
    val req = HttpFileRequest(url)

    req.use {
        val res = it.tryOpen()
        return when (res) {
            is Right<*> -> null
            is Left<*> -> req.downloadTo(location)
        }
    }
}
