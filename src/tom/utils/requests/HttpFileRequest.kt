package tom.utils.requests

import tom.utils.algebraic.Left
import tom.utils.algebraic.Result
import tom.utils.algebraic.Right
import java.io.Closeable
import java.io.File
import java.io.FileOutputStream
import java.io.IOException
import java.net.URL
import java.nio.channels.Channels
import java.nio.channels.ReadableByteChannel

/**
 * @author Thomas Povinelli
 * Created 5/31/17
 * In TomUtils
 */


open class HttpFileRequest(path: URL): AutoCloseable, Closeable {
    object Success

    var path: URL = path
        private set
    protected open var rbc: ReadableByteChannel? = null
    protected open var fos: FileOutputStream? = null

    open fun tryOpen(): Result<Success, IOException> {
        try {
            rbc = Channels.newChannel(path.openStream())
            opened = true
        } catch (e: IOException) {
            return Right(e)
        }
        return Left(Success)
    }

    private var opened: Boolean = false

    @Throws(IOException::class)
    open fun downloadTo(location: File): File {
        if (!location.isDirectory || !location.canWrite()) {
            throw IOException("Location parameter must be a writeable, " + "existing directory")
        }

        if (!opened) {
            throw IOException("FileRequest must be opened before you can download")
        }

        var repeatedNumber = 1
        val components = path.toString().split("/")
        var file = File(location.absolutePath + File.separator + components[components.size - 1])
        val filename = components[components.size - 1].split("\\.".toRegex()).dropLastWhile({ it.isEmpty() }).toTypedArray()

        var f = "aFileName"
        var ext = ""
        try {
            f = filename[0].ifEmpty { "aFileName" }
            ext = "." + filename[1]
        } catch (ignored: IndexOutOfBoundsException) {
            // Silent discard
        }


        // ENSURE WE DO NOT OVERWRITE EXTANT FILES
        while (file.exists()) {
            file = File(location.absolutePath + File.separator +
                          f + "(" + repeatedNumber++ + ")" + ext)
        }

        fos = FileOutputStream(file)
        try {
            fos?.channel?.transferFrom(rbc, 0, java.lang.Long.MAX_VALUE) ?: throw RuntimeException()
        } catch (ignored: IOException) {
            //silent discard
        }

        return file
    }

    override fun close() {
        rbc?.close()
        fos?.close()
    }

}

