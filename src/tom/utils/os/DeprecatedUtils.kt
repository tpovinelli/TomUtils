package tom.utils.os

import tom.utils.annotations.Blocking
import tom.utils.annotations.TimeoutPolicy
import java.util.concurrent.TimeUnit


typealias ExitCode = Int

@Blocking(TimeoutPolicy.CAN_TIMEOUT)
@Throws(IllegalThreadStateException::class)
@JvmOverloads
@Deprecated("getStatusOutput should be replaced by waitForStatusOutput(String) or waitForStatusOutput(String, Long, TimeUnit)", level = DeprecationLevel.WARNING)
fun getStatusOutput(
    cmd: String,
    timeOutMillis: Long = -1
): Pair<ExitCode, String> {
  val p: Process = Runtime.getRuntime().exec(cmd)
  val exitValue = if (timeOutMillis < 0) {
    p.waitFor()
  } else {
    val didExit = p.waitFor(timeOutMillis, TimeUnit.MILLISECONDS)
    if (!didExit) {
      throw IllegalThreadStateException("The process started by command \"$cmd\" did not exit within timeout $timeOutMillis millis")
    } else {
      p.exitValue()
    }
  }
  return exitValue to getProcessOutput(p)
}
