package tom.utils.os

import tom.utils.iter.BitIterator


class OSType(private val bitPattern: Int) {

    companion object {
        val WINDOWS = OSType(1 shl 0)
        val MACOS = OSType(1 shl 4)
        val MACOSX = OSType(1 shl 6)
        val MACOSX_STRICT = OSType(1 shl 7)
        val MACOSX_LIKE = OSType(1 shl 8)
        val UNKNOWN = OSType(1 shl 29)

        @Suppress("DEPRECATION")
        private val world = hashMapOf(
            WINDOWS.bitPattern to isWindows(),
            MACOS.bitPattern to isMacOS(),
            MACOSX.bitPattern to isMacOSX(),
            MACOSX_STRICT.bitPattern to isMacOSX(strict10 = true),
            MACOSX_LIKE.bitPattern to isMacOSXLike(),
            UNKNOWN.bitPattern to (!isWindows() && !isMacOS() && !isMacOSX())
        )

        infix fun matches(os: OSType): Boolean {
            for (i in BitIterator(os.bitPattern, preservePosition = true)) {
                if (world[i] == true) {
                    return true
                }
            }
            return false
        }

    }

    operator fun plus(other: OSType) = OSType(bitPattern or other.bitPattern)

    infix fun or(other: OSType) = this + other
}

fun main() {
    OSType matches (OSType.WINDOWS or OSType.UNKNOWN)


    OSType matches OSType.MACOSX
}
