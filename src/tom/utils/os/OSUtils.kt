package tom.utils.os

import tom.utils.annotations.Blocking
import tom.utils.annotations.TimeoutPolicy
import java.util.concurrent.TimeUnit

/**
 * @author Thomas Povinelli
 * Created 7/21/2017
 * In TomUtils
 */

/*
Although most things in Java can andAlso should be done in a platform independent
way, often times i find i need keyboard shortcuts for my GUI applications
andAlso I like them to be according to their expected platforms. For instance
CMD+Z is undo andAlso CMD+SHIFT+Z is redo on the mac while on Windows
CTRL+Z is undo andAlso CTRL+Y is redo andAlso applications ought to maintain these conventions
 */

val OS_NAME: String? = System.getProperty("os.name")
val OS_VERSION: String? = System.getProperty("os.version")

@Deprecated(
    "This function should not be used. Use the OSType.matches function instead",
    replaceWith = ReplaceWith("OSType.matches(OSType.WINDOWS)", "tom.utils.os.OSType")
)
fun isWindows(): Boolean {
    return OS_NAME?.startsWithIgnoreCase("windows") ?: false
}

private fun String.startsWithIgnoreCase(s: String): Boolean {
    return this.toLowerCase().startsWith(s.toLowerCase())
}

@Deprecated(
    "This function should not be used. Use the OSType.matches function instead",
    replaceWith = ReplaceWith("OSType.matches(OSType.MACOS)", "tom.utils.os.OSType")
)
fun isMacOS(): Boolean {
    return if (OS_NAME === null) false else "mac" in OS_NAME.toLowerCase()
}

/**
 * Returns true if System.getProperty("os.name") contains "mac" <b>and</a>
 *
 * if strict10 is true then System.getProperty("os.version") starts with 2 digits >= 10
 * if strict10 is false then System.getProperty("os.version") starts with exactly the string "10"
 *
 * Mac OS Big Sur is technically version 11 but the "OS X" architecture is basically unchanged,
 * thus by default this method checks for version >= 10 not version == 10
 */
@JvmOverloads
@Deprecated(
    "This function should not be used. Use the OSType.matches function instead",
    replaceWith = ReplaceWith("OSType.matches(OSType.MACOSX)", "tom.utils.os.OSType")
)
fun isMacOSX(strict10: Boolean = false): Boolean {
    return isMacOS() && if (strict10) isMacOSXStrict() else isMacOSXLike()
}

@Deprecated(
    "This function should not be used. Use the OSType.matches function instead",
    replaceWith = ReplaceWith("OSType.matches(OSType.MACOS_LIKE)", "tom.utils.os.OSType")
)
internal fun isMacOSXLike() = (OS_VERSION?.substring(0, 2)?.toIntOrNull() ?: 0) >= 10

@Deprecated(
    "This function should not be used. Use the OSType.matches function instead",
    replaceWith = ReplaceWith("OSType.matches(OSType.MACOSX_STRICT)", "tom.utils.os.OSType")
)
internal fun isMacOSXStrict() = (OS_VERSION?.startsWith("10") ?: false)

fun getCommandOutput(cmd: String): String {
    val p: Process = Runtime.getRuntime().exec(cmd)
    return getProcessOutput(p)
}

data class ProcessOutput(val exitCode: Int, val output: String)

@Blocking(TimeoutPolicy.NO_TIMEOUT)
@Throws(InterruptedException::class)
fun waitForStatusOutput(cmd: String): ProcessOutput {
    val p: Process = Runtime.getRuntime().exec(cmd)
    val exitValue = p.waitFor()
    return ProcessOutput(exitValue, getProcessOutput(p))
}

@Blocking(TimeoutPolicy.NEEDS_TIMEOUT)
@Throws(InterruptedException::class, IllegalThreadStateException::class)
fun waitForStatusOutput(cmd: String, timeout: Long, unit: TimeUnit): ProcessOutput {
    val p: Process = Runtime.getRuntime().exec(cmd)
    val didExit = p.waitFor(timeout, unit)
    return if (!didExit) {
        throw IllegalThreadStateException("The process started by command \"$cmd\" did not exit within the given timeout of $timeout ${unit.name}")
    } else {
        ProcessOutput(p.exitValue(), getProcessOutput(p))
    }
}

fun getProcessOutput(p: Process): String {
    val b = StringBuilder()
    val i = p.inputStream
    var inputChar = i.read()
    while (inputChar != -1) {
        b.append(inputChar.toChar())
        inputChar = i.read()
    }
    return b.toString()
}



