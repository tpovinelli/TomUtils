package tom.utils.kotlin

import java.math.BigDecimal
import java.math.BigInteger
import java.util.*
import java.util.function.Predicate
import java.util.function.Supplier
import kotlin.contracts.ExperimentalContracts
import kotlin.contracts.contract
import kotlin.reflect.KClass
import kotlin.reflect.typeOf

/**
 * @author Thomas Povinelli
 * Created 7/8/17
 * In TomUtils
 */

inline fun <reified T: Any> type(value: T): KClass<out T> {
    return value::class
}


/**
 * This function mimics the functionality of the guard statement
 * in Swift. The [block] passed must not return so that
 * if the [condition] is false the enclosing scope of
 * guard must be exited
 *
 * This functionality is enabled by the inline modifier which
 * inlines the function as well as the lambda. Since the lambda returns
 * [Nothing] and it is inlined, calling the lambda requires the function
 * which calls [guard] itself to not exit normally as the lambda
 * must return [Nothing]. If [guard] and [block] were not inlined
 * this would not be possible to enforce
 *
 * If/When Kotlin contracts leave experimental status,
 * contracts will be added to have the compiler enforce some
 * of the guarantees that [guard] provides
 *
 */
@OptIn(ExperimentalContracts::class)
inline fun guard(condition: Boolean, block: () -> Nothing) {
    contract { returns() implies condition }
    if (!condition) {
        block()
    }
}

/**
 * calls [Supplier.get] returning the value wrapped in an Optional
 * @return an optional containing the value of [Supplier.get] or [Optional.empty]
 *         if the result of get is null
 */
inline fun <T> safely(chainSupplier: Supplier<T>): Optional<out T> {
    return noexcept(chainSupplier, NullPointerException::class.java)
}

/**
 * Returns an optional containing the value of calling [Supplier.get] or
 * [Optional.empty] if the supplier throws an exception which is one of the types
 * provided in the exceptions argument. Otherwise it throws the exception
 * @return an Optional containing the value of the supplier or empty Optional if an exception that is listed is thrown
 * @throws Exception if an exception is thrown in [Supplier.get] and it is not in the list of exceptions provided
 */
inline fun <T> noexcept(
  chainSupplier: Supplier<T>,
  vararg exceptions: Class<*>
): Optional<out T> {
    return try {
        val result = chainSupplier.get()
        Optional.ofNullable(result)
    } catch (e: Exception) {
        if (exceptions.any { it.isInstance(e) }) {
            Optional.empty()
        } else {
            throw e
        }
    }
}

/**
 * like [noexcept] but catches all throws of type [Throwable]
 */
inline fun <T> noexcept(chainSupplier: Supplier<T>): Optional<out T> {
    return noexcept(chainSupplier, Throwable::class.java)
}

fun <T> ((T) -> Boolean).negate(): (T) -> Boolean = { !this(it) }
operator fun <T> ((T) -> Boolean).not(): (T) -> Boolean = { !this(it) }
operator fun <T> Predicate<T>.not(): (T) -> Boolean = { !this.test(it) }

fun <R> trying(block: () -> R): R? {
    return try {
        block()
    } catch (e: Exception) {
        null
    }
}

fun <R> trying(vararg toNulls: Class<out Exception>, block: () -> R): R? {
    try {
        return block()
    } catch (e: Exception) {
        for (ec in toNulls) {
            if (e::class.java == ec.javaClass) {
                return null
            }
        }
        throw e
    }
}

fun Int.toBigInteger(): BigInteger {
    return BigInteger.valueOf(this.toLong())
}

fun Int.toBigDecimal(): BigDecimal {
    return BigDecimal(this)
}

interface Booleable {
    fun booleanValue(): Boolean
}

/**
 * Return a boolean coercion of some types. For null this is false. For any
 * number-like type (including Big* classes) 0 is false. For collections
 * of any kind (map, string, array, etc) empty collection is false. Iterators
 * are false if they do not return true for hasNext. Optional's boolean value
 * is the same as [Optional.isPresent]. Classes can also implement [Booleable]
 * in which case the result of this method is the result of [Booleable.booleanValue]
 * any other classes that attempt to use this method throw a [TypeCastException]
 * @throws TypeCastException if the type cannot be turned into a boolean
 */
@Throws(TypeCastException::class)
fun <T> T?.isTruthy(): Boolean = when (this) {
    null             -> false
    is Boolean       -> this
    is Byte          -> this != 0.toByte()
    is Char          -> this != 0.toChar()
    is Short         -> this != 0.toShort()
    is Int           -> this != 0
    is Long          -> this != 0L
    is Float         -> this != 0.0F
    is Double        -> this != 0.0
    is BigDecimal    -> this != 0.toBigDecimal()
    is BigInteger    -> this != 0.toBigInteger()
    is String        -> this != ""
    is CharSequence  -> this.isNotEmpty()
    is Optional<*>   -> this.isPresent
    is Collection<*> -> this.size != 0
    is Iterable<*>   -> this.any()
    is Iterator<*>   -> this.hasNext()
    is Array<*>      -> this.isNotEmpty()
    is Map<*, *>     -> this.isNotEmpty()
    is Booleable     -> this.booleanValue()
    else             -> throw TypeCastException("Object of type ${(this as Any).javaClass} cannot be coerced to boolean")
}

/**
 * Exactly the same as [isTruthy] but returns null instead of throwing
 * if type cannot be converted to boolean value
 * @return null if type cannot be converted to boolean value, otherwise boolean value
 */
fun <T> T?.boolOrNull(): Boolean? = try {
    this.isTruthy()
} catch (t: TypeCastException) {
    null
}


inline fun <reified X: Exception> shouldFail(test: () -> Unit): Boolean {
    return try {
        test()
        false
    } catch (e: Exception) {
        e is X
    }
}


class FatalError(
  override val cause: Throwable?,
  override val message: String?
): Error(message, cause) {
    constructor(message: String?): this(null, message)
}

@Throws(FatalError::class)
@JvmOverloads
fun fatalError(cause: Throwable? = null, lazyMessage: () -> Any): Nothing {
    val msg = lazyMessage()
    throw FatalError(cause, msg.toString())
}

@Throws(FatalError::class)
inline fun die(lazyMessage: () -> Any): Nothing {
    val message = lazyMessage().toString()
    throw FatalError(message)
}

@Throws(FatalError::class)
infix fun <T> T.die(message: String): Nothing {
    throw FatalError(message)
}

@Throws(FatalError::class)
fun die(): Nothing {
    throw FatalError("")
}
