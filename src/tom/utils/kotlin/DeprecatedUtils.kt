package tom.utils.kotlin

import java.util.*
import java.util.function.Supplier


@Deprecated(
    message = "Since Kotlin 1.1 String.toIntOrNull() exists in the standard library and should be used instead",
    level = DeprecationLevel.WARNING,
    replaceWith = ReplaceWith("s.toIntOrNull()")
)
fun parseInt(s: String): Int? {
    return try {
        Integer.parseInt(s)
    } catch (n: NumberFormatException) {
        null
    }
}

/**
 * Performs the same function as [safely] but returns a raw value or null
 * instead of the corresponding Optional wrapping
 */
@Deprecated(
    "Insufficient justification for continued existence, this function WILL be removed",
    level = DeprecationLevel.WARNING
)
inline fun <T> resolve(chainSupplier: Supplier<T>): T? {
    return try {
        val result = chainSupplier.get()
        result
    } catch (npe: NullPointerException) {
        null
    }
}

@Deprecated(
    "Insufficient justification for continued existence, this function WILL be removed",
    level = DeprecationLevel.WARNING,
    replaceWith = ReplaceWith("orElse(other)")
)
infix fun <T> Optional<T>.or(other: T): T {
    return orElse(other)
}

@Deprecated(
    "Insufficient justification for continued existence, this function WILL be removed",
    ReplaceWith("orElse(other)"),
    DeprecationLevel.WARNING
)
infix fun OptionalDouble.or(other: Double): Double {
    return orElse(other)
}

@Deprecated(
    "Insufficient justification for continued existence, this function WILL be removed",
    ReplaceWith("orElse(other)"),
    DeprecationLevel.WARNING
)
infix fun OptionalInt.or(other: Int): Int {
    return orElse(other)
}

@Deprecated(
    "Insufficient justification for continued existence, this function WILL be removed",
    ReplaceWith("orElse(other)"),
    DeprecationLevel.WARNING
)
infix fun OptionalLong.or(other: Long): Long {
    return orElse(other)
}


@Deprecated(
    "Use the wrapper trying(() -> R) instead",
    replaceWith = ReplaceWith("trying(this)"),
    level = DeprecationLevel.WARNING
)
fun <R> (() -> R).exceptionsAsNull(): R? {
    return try {
        this()
    } catch (t: Exception) {
        null
    }
}

@Deprecated("This function is unnecessary. Try and use the ?: operator instead", level = DeprecationLevel.WARNING)
inline infix fun <R> (() -> R).otherwise(block: () -> R): R {
    val result: R? = try {
        this()
    } catch (e: Exception) {
        null
    }
    if (result.isTruthy()) {
        return result!!
    }
    return block()
}


@Deprecated("This function is unnecessary. Try and use the ?: operator instead", level = DeprecationLevel.WARNING)
infix fun <R> (() -> R).otherwiseTry(block: () -> R): () -> R {
    val result = try {
        this()
    } catch (e: Exception) {
        null
    }
    if (result.isTruthy() && result != null) {
        return { result }
    }
    return block
}
