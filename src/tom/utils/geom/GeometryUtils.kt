package tom.utils.geom

import kotlin.math.sqrt

/**
 * @author Thomas Povinelli
 * Created 2018-May-12
 * In TomUtils
 */

fun euclideanDistance(x1: Double, y1: Double, x2: Double, y2: Double): Double {
    return sqrt(((x1 - y1) * (x1 - y1)) + ((x2 - y2) * (x2 - y2)))
}

fun Int.map(os: Int, oe: Int, ns: Int, ne: Int): Double {
    return this.map(os, oe, ns.toDouble(), ne.toDouble())
}

fun Int.map(os: Int, oe: Int, ns: Double, ne: Double): Double {
    return ns + (((this - os).toDouble() / (oe - os)) * (ne - ns))
}


fun Int.map(from: IntRange, to: IntRange): Double {
    val os = from.start
    val oe = from.endInclusive
    val ns = to.start
    val ne = to.endInclusive
    return this.map(os, oe, ns, ne)
}

fun Double.map(os: Double, oe: Double, ns: Double, ne: Double): Double {
    return ns + (((this - os) / (oe - os)) * (ne - ns))
}

fun Float.map(os: Float, oe: Float, ns: Float, ne: Float): Float {
    return ns + (((this - os) / (oe - os)) * (ne - ns))
}

fun Byte.map(os: Byte, oe: Byte, ns: Byte, ne: Byte): Byte {
    return (ns + (((this - os) / (oe - os)) * (ne - ns))).toByte()
}

fun Char.map(os: Char, oe: Char, ns: Char, ne: Char): Char {
    return (ns + (((this - os) / (oe - os)) * (ne - ns)))
}

fun Char.map(from: CharRange, to: CharRange): Char {
    val os = from.start
    val oe = from.endInclusive
    val ns = to.start
    val ne = to.endInclusive
    return this.map(os, oe, ns, ne)
}

fun Long.map(os: Long, oe: Long, ns: Long, ne: Long): Double {
    return this.map(os, oe, ns.toDouble(), ne.toDouble())
}

fun Long.map(os: Long, oe: Long, ns: Double, ne: Double): Double {
    return ns + (((this - os) / (oe - os)) * (ne - ns))
}

fun Long.map(from: LongRange, to: LongRange): Double {
    val os = from.start
    val oe = from.endInclusive
    val ns = to.start
    val ne = to.endInclusive
    return this.map(os, oe, ns, ne)
}

fun Long.map(from: LongRange, ns: Double, ne: Double): Double {
    val os = from.start
    val oe = from.endInclusive
    return this.map(os, oe, ns, ne)
}
