package tom.utils.time

import java.time.Duration
import java.time.LocalDateTime
import java.time.temporal.ChronoUnit
import java.util.function.Supplier

data class TimedResult<out R>(val results: List<R>, val durations: List<Duration>) {
    fun averageDuration(): Duration {
        val average = durations.stream()
            .mapToLong { it[ChronoUnit.NANOS] }
            .average()
            .orElse(-1.0)
            .toLong()
        return Duration.of(average, ChronoUnit.NANOS)
    }

    fun firstResult(): R? = results.firstOrNull()
}

inline fun <reified T : Number> TimedResult<T>.averageResult(): Double {
    return when (T::class.java) {
        Int::class.java -> (results as List<Int>).average()
        Short::class.java -> (results as List<Short>).average()
        Char::class.java -> (results as List<Char>).map { it.code }.average()
        Byte::class.java -> (results as List<Byte>).average()
        Double::class.java -> (results as List<Double>).average()
        Float::class.java -> (results as List<Float>).average()
        Long::class.java -> (results as List<Long>).average()
        else -> throw TypeCastException("Not a subclass of number: ${T::class.java.name}")
    }
}

fun <R> timeOnce(block: () -> R): Pair<R, Duration> {
    val (results, durations) = time(1, block)
    return results.first() to durations.first()
}

fun <R> time(avgOverIterations: Int = 1, block: () -> R): TimedResult<R> {
    val results = mutableListOf<R>()
    val durations = mutableListOf<Duration>()
    for (i in 1..avgOverIterations) {
        val start = System.currentTimeMillis()
        val r = block()
        val end = System.currentTimeMillis()
        results.add(r)
        durations.add(Duration.of(end - start, ChronoUnit.MILLIS))
    }
    return TimedResult(results, durations)
}

fun <R> timeOnce(block: Supplier<R>): Pair<R, Duration> {
    return timeOnce { block.get() }
}

@JvmOverloads
fun <R> time(avgOverIterations: Int = 1, block: Supplier<R>): TimedResult<R> {
    return time(avgOverIterations) {
        block.get()
    }
}

sealed class TimeUnit(open val n: Int)
sealed class TimeObject

data class Day(override val n: Int) : TimeUnit(n)
data class Hour(override val n: Int) : TimeUnit(n)
data class Minute(override val n: Int) : TimeUnit(n)
data class Second(override val n: Int) : TimeUnit(n)

object Days : TimeObject()
object Hours : TimeObject()
object Minutes : TimeObject()
object Seconds : TimeObject()

val Int.days: Day get() = Day(this)
val Int.hours: Hour get() = Hour(this)
val Int.minutes: Minute get() = Minute(this)
val Int.seconds: Second get() = Second(this)

val Int.day: Day get() = Day(this)
val Int.hour: Hour get() = Hour(this)
val Int.minute: Minute get() = Minute(this)
val Int.second: Second get() = Second(this)

infix fun TimeUnit.after(ldt: LocalDateTime): LocalDateTime {
    return when (this) {
        is Day -> ldt.plusDays(n.toLong())
        is Hour -> ldt.plusHours(n.toLong())
        is Minute -> ldt.plusMinutes(n.toLong())
        is Second -> ldt.plusSeconds(n.toLong())
    }
}

infix fun TimeUnit.before(ldt: LocalDateTime): LocalDateTime {
    return when (this) {
        is Day -> ldt.minusDays(n.toLong())
        is Hour -> ldt.minusHours(n.toLong())
        is Minute -> ldt.minusMinutes(n.toLong())
        is Second -> ldt.minusSeconds(n.toLong())
    }
}

infix fun TimeObject.since(localDateTime: LocalDateTime): Long {
    return when (this) {
        is Days -> localDateTime.until(LocalDateTime.now(), ChronoUnit.DAYS)
        is Hours -> localDateTime.until(LocalDateTime.now(), ChronoUnit.HOURS)
        is Minutes -> localDateTime.until(LocalDateTime.now(), ChronoUnit.MINUTES)
        is Seconds -> localDateTime.until(LocalDateTime.now(), ChronoUnit.SECONDS)
    }
}

infix fun TimeObject.till(localDateTime: LocalDateTime): Long {
    return when (this) {
        is Days -> ChronoUnit.DAYS.between(LocalDateTime.now(), localDateTime)
        is Hours -> ChronoUnit.HOURS.between(LocalDateTime.now(), localDateTime)
        is Minutes -> ChronoUnit.MINUTES.between(LocalDateTime.now(), localDateTime)
        is Seconds -> ChronoUnit.SECONDS.between(LocalDateTime.now(), localDateTime)
    }
}

fun main() {
    val lastWeek = 7.days before LocalDateTime.now()
    println(1.day after LocalDateTime.now())
    println(Minutes since lastWeek)
}
