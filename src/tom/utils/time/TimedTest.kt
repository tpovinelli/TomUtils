package tom.utils.time

import java.util.*
import java.util.concurrent.TimeUnit


abstract class TimedTest {
    var start: Long = 0
        private set
    var end: Long = 0
        private set
    var isResultReady: Boolean = false
        private set

    abstract fun buildUp()
    abstract fun tearDown()

    abstract fun test()

    fun performTest() {
        buildUp()
        start = System.currentTimeMillis()
        test()
        end = System.currentTimeMillis()
        tearDown()
        isResultReady = true
    }

    class Results(val start: Long, val end: Long, val timeUnit: TimeUnit) {
        val elapsed: Long get() = end - start

        fun elapsedIn(unit: TimeUnit): Long {
            return unit.convert(elapsed, TimeUnit.MILLISECONDS)
        }

        fun startIn(unit: TimeUnit): Long {
            return unit.convert(start, TimeUnit.MILLISECONDS)
        }

        fun endIn(unit: TimeUnit): Long {
            return unit.convert(end, TimeUnit.MILLISECONDS)
        }
    }

    fun results(): TimedTest.Results {
        return Results(start, end, TimeUnit.MILLISECONDS)
    }

}

class MapForTest : TimedTest() {

    val r = Random()
    val a = Array(100000000) {
        r.nextInt(100000000)
    }

    override fun buildUp() {
    }

    override fun tearDown() {
    }

    override fun test() {
        var total = 0
        for (i in a) {
            if (i == 0) {
                continue
            }
            total += i
        }
        val avg = total.toDouble() / a.size
        println(avg)
    }
}

fun main() {
    val t = MapForTest()
    t.performTest()
    val r = t.results()
    println("MapForTest took ${r.elapsedIn(TimeUnit.SECONDS)} seconds to get average")
}
