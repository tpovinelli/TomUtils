package tom.utils.file

import org.jetbrains.annotations.Contract
import java.io.File
import java.io.IOException
import java.net.URI
import java.net.URISyntaxException
import java.net.URL
import java.nio.file.Files
import kotlin.contracts.ExperimentalContracts
import kotlin.contracts.contract

fun String.toFile(): File {
    return File(this)
}

fun URL.toFile(): File? {
    return try {
        this.toURI().toFile()
    } catch (e: Exception) {
        when (e) {
            is IOException, is URISyntaxException -> null
            else -> throw e
        }
    }
}

fun URI.toFile(): File {
    return File(this)
}

@OptIn(ExperimentalContracts::class)
internal fun checkIO(condition: Boolean, message: () -> String) {
    contract { returns() implies condition }
    if (!condition) throw IOException(message())
}

@Deprecated(
    "containsFile does not check for file existence. Use isSubpath or containsExistingFile",
    replaceWith = ReplaceWith("isSubpath(other)")
)
@Throws(IOException::class)
@Contract(pure = true)
infix fun File.containsFile(other: File?): Boolean {
    checkIO(!isDirectory) { "File <${this.absolutePath}> is not a directory. Only directories contain files" }
    return this.isSubpathOf(other)
}

/**
 * Same as `File.containsFile(other)` but has a better name. Does NOT check to see if `other` exists
 */
tailrec infix fun File.isSubpathOf(other: File?): Boolean {
    other ?: return false

    return if (other == this) {
        true
    } else {
        this isSubpathOf other.parentFile
    }
}

/**
 * Works like isSubpathOf but checks the file exists
 */
infix fun File.containsExistingFile(other: File?): Boolean {
    return other != null && other.exists() && with(this) { exists() && isDirectory && isSubpathOf(other) }
}

@Throws(IOException::class)
fun File.readBytes(): ByteArray {
    val path = this.toPath()
    return Files.readAllBytes(path)
}

// NO longer deprecated because File.readText() does not exist in Java
//@Deprecated(message = "Use stdlib File.readText() instead", replaceWith = ReplaceWith("readText()"))
@get:Throws(IOException::class)
val File.text: String
    get() = this.readText()
