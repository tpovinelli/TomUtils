package tom.utils.array

/**
 * @author Thomas Povinelli
 * Created 7/5/17
 * In TomUtils
 */


import tom.utils.string.plusAssign

@JvmOverloads
fun ByteArray.toPrintableString(compact: Boolean = true, terminalWidth: Int = -1): String =
    toTypedArray().toPrintableString(compact, terminalWidth)

@JvmOverloads
fun IntArray.toPrintableString(compact: Boolean = true, terminalWidth: Int = -1): String =
    toTypedArray().toPrintableString(compact, terminalWidth)

@JvmOverloads
fun CharArray.toPrintableString(compact: Boolean = true, terminalWidth: Int = -1): String =
    toTypedArray().toPrintableString(compact, terminalWidth)

@JvmOverloads
fun DoubleArray.toPrintableString(compact: Boolean = true, terminalWidth: Int = -1): String =
    toTypedArray().toPrintableString(compact, terminalWidth)

@JvmOverloads
fun LongArray.toPrintableString(compact: Boolean = true, terminalWidth: Int = -1): String =
    toTypedArray().toPrintableString(compact, terminalWidth)

@JvmOverloads
fun FloatArray.toPrintableString(compact: Boolean = true, terminalWidth: Int = -1): String =
    toTypedArray().toPrintableString(compact, terminalWidth)

@JvmOverloads
fun BooleanArray.toPrintableString(compact: Boolean = true, terminalWidth: Int = -1): String =
    toTypedArray().toPrintableString(compact, terminalWidth)

@JvmOverloads
fun <T> Array<T>.toPrintableString(compact: Boolean = true, terminalWidth: Int = -1): String {
    if (terminalWidth != -1 && terminalWidth < 4) {
        throw IllegalArgumentException("Terminal width of $terminalWidth is invalid. Must be >= 4")
    }
    val s = StringBuilder()
    val width = if (terminalWidth == -1) Int.MAX_VALUE else (if (terminalWidth < 5) 5 else terminalWidth)
    s += if (compact) "{" else "{\n  "
    var currentLine = StringBuilder()
    for (i in this) {
        val currentElt = i.toString()
        // currentLine + currentElement + comma + space
        if (currentLine.length + currentElt.length + 1 + 1 >= width) {
            currentLine.deleteCharAt(currentLine.lastIndex)
            s += currentLine.toString()
            s += "\n"
            currentLine = StringBuilder(if (compact) "" else "  ")
            currentLine += currentElt
            currentLine += ", "
        } else {
            currentLine += currentElt
            currentLine += ", "
        }
    }
    s += currentLine.toString()
    s.delete(s.length - 2, s.length)
    s += if (compact) "}" else "\n}"
    return s.toString()
}
