package tom.utils.algebraic


sealed class Result<out L, out R> where R : Exception {
    val isLeft: Boolean get() = this is Left<L>
    val isRight: Boolean get() = this is Right<R>

//    fun asLeft(): Left<L> = this as Left<L>
//    fun asLeftOrNull(): Left<L>? = this as? Left<L>

    fun <N, X : Exception> then(block: () -> Result<N, X>): Result<N, Exception> {
        return when (this) {
            is Right<*> -> Right(exception)
            is Left<L> -> block()
        }
    }

    @JvmName("thenFrom")
    fun <N> then(block: () -> N): Result<N, Exception> {
        return when (this) {
            is Right<*> -> Right(exception)
            is Left<L> -> {
                try {
                    Left(block())
                } catch (e: Exception) {
                    Right(e)
                }
            }
        }
    }

    fun getOrThrow(): L {
        return when (this) {
            is Left<L> -> value
            is Right<*> -> fail()
        }
    }

    fun getOrElse(alt: @UnsafeVariance L): L {
        return when (this) {
            is Left<L> -> value
            is Right<*> -> alt
        }
    }

    fun asOptional(): L? {
        return if (this is Left<L>) value else null
    }

    companion object {
        infix fun <T> from(throwingBlock: () -> T) = squash(throwingBlock)

        fun <T> squash(throwingBlock: () -> T): Result<T, Exception> {
            return try {
                Left(throwingBlock())
            } catch (e: Exception) {
                Right(e)
            }
        }
    }
}

fun <T> (() -> T).result(): Result<T, Exception> {
    return try {
        Left(this())
    } catch (e: Exception) {
        Right(e)
    }
}

fun <T> (() -> T).resultify(): () -> Result<T, Exception> {
    return { this.result() }
}

data class Left<L>(val value: L) : Result<L, Nothing>()

data class Right<R>(val exception: R) : Result<Nothing, R>() where R : Exception {
    fun fail(): Nothing {
        throw exception
    }
}

fun main() {
    
}