package tom.utils.thread;

/**
 * @author Thomas Povinelli
 * Created 2018-Jan-06
 * In TomUtils
 */

public abstract class JobThread extends Thread {

    private final Thread spawner;
    private final Runnable job;
    protected JobThreadType type;

    protected JobThread(Thread spawner, Runnable job) {
        /* if you erase this one line NOTHING about this class or its subclasses work at all */
        super(job);
        this.spawner = spawner;
        this.job = job;
    }

    public final String name() {
        return typeName() + " Job Thread (" + super.getName() +
               ") [spawner: " +
               spawner.getName() + "]";
    }

    private String typeName() {
        return switch (type) {
            case CONCURRENT -> "Concurrent";
            case BLOCKING -> "Blocking";
        };
    }

    public final String fullName() {
        if (spawner instanceof JobThread) {
            return typeName() + " Job Thread (" + super.getName() +
                   ") [spawner: " +
                   ((JobThread) spawner).fullName() + "]";
        } else {
            return typeName() + " Job Thread (" + super.getName() +
                   ") [spawner: " +
                   spawner.getName() + "]";
        }
    }

    protected enum JobThreadType {
        CONCURRENT, BLOCKING
    }
}
