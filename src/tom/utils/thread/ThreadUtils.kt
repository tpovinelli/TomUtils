package tom.utils.thread

import java.util.concurrent.TimeUnit
import java.util.concurrent.locks.ReentrantLock
import kotlin.concurrent.withLock

fun runAfter(timeout: Long, timeUnit: TimeUnit, block: () -> Unit) {
    concurrently {
        Thread.sleep(timeUnit.toMillis(timeout))
        block()
    }
}

fun sleep(timeout: Long, timeUnit: TimeUnit, onInterrupt: () -> Unit) {
    concurrently {
        try {
            Thread.sleep(timeUnit.toMillis(timeout))
        } catch (e: InterruptedException) {
            onInterrupt()
        }
    }
}


fun sleep(timeout: Long, timeUnit: TimeUnit, ifInterrupted: () -> Unit, orElse: () -> Unit) {
    concurrently {
        try {
            Thread.sleep(timeUnit.toMillis(timeout))
        } catch (e: InterruptedException) {
            ifInterrupted()
            return@concurrently
        }
        orElse()
    }
}

@JvmOverloads
fun spawnJob(daemon: Boolean = false, job: () -> Unit): ConcurrentJobThread {
    return ConcurrentJobThread(Thread.currentThread(), job, daemon, true)
}

@JvmOverloads
fun spawnJob(daemon: Boolean = false, job: Runnable): ConcurrentJobThread {
    return ConcurrentJobThread(Thread.currentThread(), job, daemon, true)
}

@JvmOverloads
fun syncJob(daemon: Boolean = false, job: () -> Unit): BlockingJobThread {
    return BlockingJobThread(Thread.currentThread(), job, daemon, true)
}

@JvmOverloads
fun syncJob(daemon: Boolean = false, job: Runnable): BlockingJobThread {
    return BlockingJobThread(Thread.currentThread(), job, daemon, true)
}

@JvmOverloads
fun concurrently(daemon: Boolean = false, job: () -> Unit): Thread {
    val t = Thread(job)
    t.isDaemon = daemon
    t.name = "Concurrent Job Thread (${t.name}) [spawner: ${Thread.currentThread().name}]"
    t.start()
    return t
}

@JvmOverloads
fun concurrently(daemon: Boolean = false, job: Runnable): Thread {
    val t = Thread(job)
    t.isDaemon = daemon
    t.name = "Concurrent Job Thread (${t.name}) [spawner: ${Thread.currentThread().name}]"
    t.start()
    return t
}

fun waitFor(job: () -> Unit): Thread {
    val lock = ReentrantLock()
    val cond = lock.newCondition()
    val t = Thread {
        lock.withLock {
            job()
            cond.signal()
        }
    }
    t.name = "Blocking Job Thread (${t.name}) [spawner: ${Thread.currentThread().name}]"
    t.start()
    lock.withLock {
        while (t.isAlive) {
            cond.await()
        }
    }
    return t
}

fun waitFor(job: Runnable): Thread {
    val lock = ReentrantLock()
    val cond = lock.newCondition()
    val t = Thread {
        lock.withLock {
            job.run()
            cond.signal()
        }
    }
    t.name = "Blocking Job Thread (${t.name}) [spawner: ${Thread.currentThread().name}]"
    t.start()
    lock.lock()
    lock.withLock {
        while (t.isAlive) {
            cond.await()
        }
    }
    return t
}
