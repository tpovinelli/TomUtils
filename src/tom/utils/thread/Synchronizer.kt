package tom.utils.thread

import tom.utils.annotations.ConstructInPlace

/**
 * @author Thomas Povinelli
 * Created 10/2/19
 * In TomUtils
 */

/**
 * Provides thread-safe resource management on a specific object
 *
 * This class wraps a reference to an object and then provides thread
 * safe access to it. This is accomplished with Kotlin's [synchronized]
 * function which uses Java's moniterEnter and moniterExit methods. Essentially
 * it uses the Java primative synchronized(lock) { }
 *
 * When constructing the class you must specify the resource to be synchronized
 * and you may choose to provide an object on which to lock for the
 * synchronization. If an object is passed as the lock, then that is used to synchronize
 * access to the resource. If no lock is specified or null is passed
 * for the lock parameter, then the Synchronizer instance itself is used
 * to synchronize access to the resource.
 * It is possible to pass the resource twice and use the resource itself as the
 * lock but this is not recommended as it is recommended to construct the
 * resource in place. That is, create the resource in the constructor for
 * Synchronizer. This ensures the resource is never accessed or leaked
 * outside of the synchronized context
 */
class Synchronizer<out T: Any>(@PublishedApi internal val lock: Any?, @ConstructInPlace @PublishedApi internal val resource: T) {

    constructor(@ConstructInPlace resource: T): this(null, resource)

    inline fun <R> withResource(block: T.() -> R): R {
        return synchronized(lock ?: this) {
            resource.block()
        }
    }

    inline fun <R> usingResource(block: (T) -> R): R {
        return synchronized(lock ?: this) {
            block(resource)
        }
    }

    inline fun applyToResource(block: T.() -> Unit): Synchronizer<T> {
        synchronized(lock ?: this) {
            block(resource)
        }
        return this
    }
}
