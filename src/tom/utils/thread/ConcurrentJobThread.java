package tom.utils.thread;

/**
 * @author Thomas Povinelli
 * Created 2018-Jan-06
 * In TomUtils
 */
public class ConcurrentJobThread extends JobThread {

    public ConcurrentJobThread(Runnable job, boolean daemon, boolean start) {
        this(Thread.currentThread(), job, daemon, start);
    }

    public ConcurrentJobThread(Thread spawner, Runnable job, boolean daemon,
                               boolean start)
    {
        super(spawner, job);
        type = JobThreadType.CONCURRENT;
        super.setDaemon(daemon);
        if (start) {
            this.start();
        }
    }

    public ConcurrentJobThread(Runnable job) {
        this(Thread.currentThread(), job, false, true);
    }
}
