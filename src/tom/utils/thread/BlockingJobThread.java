package tom.utils.thread;

import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.ReentrantLock;

public class BlockingJobThread extends JobThread {
    private final AtomicBoolean living = new AtomicBoolean(false);
    private final ReentrantLock lock = new ReentrantLock();
    private final Condition condition = lock.newCondition();

    public BlockingJobThread(Runnable job) {
        this(Thread.currentThread(), job, false, true);
    }

    public BlockingJobThread(Thread spawner, Runnable job, boolean daemon,
                             boolean start)
    {
        super(spawner, job);
        super.setDaemon(daemon);
        type = JobThreadType.BLOCKING;
        if (start) {
            this.start();
        }
    }

    @Override
    public void start() {
        lock.lock();
        living.set(true);
        try {
            super.start();
            while (living.compareAndSet(true, true)) {
                condition.await();
            }
        } catch ( InterruptedException e ) {
            super.interrupt(); // TODO: needed?
        } finally {
            lock.unlock();
        }
    }

    @Override
    public void run() {
        super.run();
        living.compareAndSet(true, false);
        lock.lock();
        try {
            condition.signalAll();
        } finally {
            lock.unlock();
        }
    }

    public BlockingJobThread(Runnable job, boolean daemon, boolean start) {
        this(Thread.currentThread(), job, daemon, start);
    }

    public boolean isLiving() {
        return living.get();
    }

    public AtomicBoolean getLiving() {
        return living;
    }
}
