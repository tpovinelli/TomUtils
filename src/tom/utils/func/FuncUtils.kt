package tom.utils.func

import java.util.function.Predicate

infix fun <T, U, V> ((T) -> U).andThen(f: ((U) -> V)): (T) -> V {
    return { f(this(it)) }
}

infix fun <T, U, V> ((T) -> V).butFirst(f: ((U) -> T)): (U) -> V {
    return { this(f(it)) }
}

fun <A, B> ((A) -> B).curry(a: A): () -> B = { this(a) }

fun <A, B, C> ((A, B) -> C).curry(a: A): (b: B) -> C = { this(a, it) }

fun <A, B, C, D> ((A, B, C) -> D).curry(a: A): (b: B, c: C) -> D = { b, c -> this(a, b, c) }

fun <A, B, C> ((A, B) -> C).metacurry(b: B): (a: A) -> C = { this(it, b) }

fun <A, B, C> ((A, B) -> C).juxtapose(): (b: B, a: A) -> C = { b: B, a: A -> this(a, b) }

fun <A, B, C> ((A) -> ((B) -> C)).uncurry(): (a: A, b: B) -> C = { a: A, b: B -> this(a)(b) }


class Testing {
    val name: String by lazy { "Hello" }
}

internal fun <T> ((T) -> Boolean).jpredicate(): Predicate<T> = Predicate { this(it) }

class DescribedPredicate<T>(val description: String, val predicate: (T) -> Boolean): ((T) -> Boolean) by predicate, Predicate<T> by predicate.jpredicate()
