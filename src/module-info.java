module TomUtils {
  requires kotlin.stdlib;
  requires jdk.unsupported;
  requires org.jetbrains.annotations;

  exports tom.utils.aggregation;
  exports tom.utils.annotations;
  exports tom.utils.array;
  exports tom.utils.delegates;
  exports tom.utils.file;
  exports tom.utils.func;
  exports tom.utils.geom;
  exports tom.utils.iter.mutable;
  exports tom.utils.iter;
  exports tom.utils.kotlin;
  exports tom.utils.math;
  exports tom.utils.memory;
  exports tom.utils.os;
  exports tom.utils.reflection;
  exports tom.utils.requests;
  exports tom.utils.string;
  exports tom.utils.thread;
  exports tom.utils.time;
}