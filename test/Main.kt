import java.util.function.Supplier

sealed class Optional<out T: Any> {
    val isPresent: Boolean get() = this is Some<T>
}

class Some<out T: Any>(val value: T): Optional<T>() {
    override fun toString() = "Optional<${value::class.java.name}>($value)"
}

object None: Optional<Nothing>() {
    override fun toString() = "Optional<Nothing>()"
}

fun <T: Any> Optional<T>.valueOrElse(other: T): T = (this as? Some<T>)?.value
  ?: other

fun <T: Any> Optional<T>.valueOrGet(supplier: () -> T): T = valueOrElse(supplier())
fun <T: Any> Optional<T>.valueOrGet(supplier: Supplier<T>): T = valueOrElse(supplier.get())

fun <T: Any, R: Any> Optional<T>.ifPresent(operator: (T) -> R): Optional<R> {
    return Some((this as? Some<T>)?.value?.let(operator) ?: return None)
}

fun <T: Any, R: Any> Optional<T>.ifPresentFlat(operator: (T) -> R?): R? =
  (this as? Some<T>)?.value?.let(operator)

fun <T: Any> Optional<T>.valueOrThrow(): T = (this as? Some<T>)?.value
  ?: throw NullPointerException("Optional value not present")

val <T: Any> Optional<T>.maybe: T? get() = (this as? Some<T>)?.value

/* ************************************************************************** */

data class Person(val name: String, var parent: Optional<Person>) {
    companion object {
        private val register: MutableMap<String, Person> = mutableMapOf()

        fun named(name: String): Person =
          register[name] ?: register.let { map ->
              Person(name, None).apply {
                  map[name] = this
              }
          }
    }
}


fun main() {
    val c = Person("Thomas", None)
    val d = Person("John", Some(c))

    val p = Person.named("Thomas")
    println(c === p)
    val k = Person.named("Ryan")
    println(k)
    println(k == d)
    println(d)
}
