import tom.utils.thread.Synchronizer
import kotlin.random.Random.Default.nextInt

typealias ListGuard = Synchronizer<MutableList<Int>>

fun main() {
    val s = Synchronizer(MutableList(3000) { nextInt(1, 10000) })

    val threads = mutableListOf<Thread>()

    repeat(5) {
        threads.add(makeThread(s) { removeAt(0) })
        threads.add(makeThread(s) { add(nextInt(2, 9999)) })
    }

    for (thread in threads)
        thread.join()

    s.withResource { println(size) }
}

private fun makeThread(s: ListGuard, action: MutableList<Int>.() -> Unit) =
  Thread {
      s.withResource {
          for (i in 0 until 200)
              this.action()
      }
  }.apply { isDaemon = false; start() }
