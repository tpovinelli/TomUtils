package tom.fonts

import tom.utils.kotlin.trying

/**
 * @author Thomas Povinelli
 * Created 10/14/19
 * In TomUtils
 */

private tailrec fun matches(s: String, p: String, si: Int, pi: Int, inStar: Boolean): Boolean {
    if (pi == p.length) return si == s.length

    if (si == s.length) {
        return when {
            inStar && pi < p.length -> matches(s, p, si, pi + 1, inStar)
            inStar                  -> true
            else                    -> false
        }
    }

    val inStar: Boolean = trying { p[pi + 1] } == '*'
    val patternChar = p[pi]
    val stringChar = s[si]
    val charsMatch = (stringChar == patternChar || patternChar == '.')

    return when {
        charsMatch && inStar            -> matches(s, p, si + 1, pi, inStar)
        charsMatch && !inStar           -> matches(s, p, si + 1, pi + 1, inStar)
        !charsMatch && inStar           -> matches(s, p, si, pi + 2, inStar)
        else /*!charsMatch && !inStar*/ -> false
    }
}

infix fun String.matches(pattern: String) = matches(this, pattern, 0, 0, false)

fun main() {
//    assertFalse("aba" matches "ab")
//    assertTrue("aa" matches "a*a")
//    assertTrue("aa" matches "a*")
//    assertTrue("ab" matches ".*")
//    assertFalse("ab" matches ".")
//    assertTrue("aab" matches "c*a*b*")
//    assertTrue("aaa" matches "a*.")
//    assertFalse("bbbbc" matches "b*b")
//    assertTrue("bbbb" matches "b*b")
//    assertTrue("bbbbcbb" matches "b*.*b*bbb")
//    assertTrue("aaaaaaaaaa" matches "a*")
//    assertFalse("aaaa" matches "aaaaaaa")

}
