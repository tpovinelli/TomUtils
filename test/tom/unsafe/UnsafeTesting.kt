package tom.unsafe

import tom.utils.memory.RCMemoryBlock
import tom.utils.memory.zero
import kotlin.concurrent.thread

fun main() {
    fun increment(data: RCMemoryBlock, index: Long, times: Int) {
        data.retain()
        repeat(times) {
            data[index]++
            Thread.sleep(30)
        }
        println(data[index])
        data.release()
    }

    val data = RCMemoryBlock.allocate(12L).zero()

    repeat(12) {
        thread(start = true) { increment(data, it.toLong(), it * 10) }
    }

    data.release()
}
