package tom.etc;

public class ListSwap<T> {
    Node<T> head;
    int size;

    public static void main(String[] args) {
        ListSwap<Integer> l = new ListSwap<>();
        l.add(1);
        l.add(2);
        l.add(4);
        l.add(3);
        l.add(5);
        System.out.println(l);
        l.swap(4);
        System.out.println(l);

    }

    public void add(T data) {
        if (head == null) {
            head = new Node<>(data);
        } else {
            Node<T> crawler = head;
            while (crawler.next != null) {
                crawler = crawler.next;
            }
            crawler.next = new Node<>(data);
        }
    }

    public T get(int index) {
        Node<T> crawler = head;
        while (index-- > 0) {
            crawler = crawler.next;
        }
        return crawler.data;
    }

    public void swap(T data) {
        Node<T> crawler = head;
        while (crawler.next != null && !crawler.next.data.equals(data)) {
            crawler = crawler.next;
        }
        Node<T> target = crawler.next;
        Node<T> nextTarger = crawler.next;
        Node<T> last = crawler.next;

        crawler.next = nextTarger;
        nextTarger.next = target;
        target.next = last;
    }

    @Override
    public String toString() {
        return head.toString();
    }

    class Node<T> {
        T data;
        Node<T> next;

        public Node(T data) {
            this.data = data;
        }

        @Override
        public String toString() {
            return data.toString() + " " + (next == null ? "" : next.toString());
        }
    }
}
