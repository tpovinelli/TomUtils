package tom.etc;

/**
 * @author Thomas Povinelli
 *
 * This class encapsulates the idea of a Point which consists of two doubles,
 * an x and a y coordinate on the Cartesian plane.
 *
 * Points are mutable and have getters, setters, tom.etc.equals, and clone method.
 *
 * this class was created to demonstrate Javadoc, clone, and tom.etc.equals
 *
 */
public class Point implements Cloneable {

    private double x;
    private double y;

    /**
     * Construct a new Point with the given x and y coordinates
     */
    public Point(double x, double y) { this.x = x; this.y = y; }

    /**
     * Return the x part of the coordinate represented by the point
     * @return the Point's x coordinate
     */
    public double getX() { return x; }

    /**
     * Return the y part of the coordinate represented by the point
     * @return the Point's y coordinate
     */
    public double getY() { return y; }

    /**
     * Sets the Point's x coordinate to the value passed as a parameter
     * @param x the new x coordinate of the Point
     */
    public void setX(double x) { this.x = x; }

    /**
     * Sets the Point's y coordinate to the value passed as a parameter
     * @param y the new y coordinate of the Point
     */
    public void setY(double y) { this.y = y; }

    /**
     * Test if two points are equal by comparing if the
     * Points' x coordinates are the same and if the Points'
     * y coordinates are the same.
     * @return true if the 2 Points' x and y coordinates are the same, false otherwise
     */
    public boolean equals(Object other) {
        if (other == this) {
            return true;
        }
        if (!(other instanceof Point)) {
            return false;
        }

        Point that = (Point) other;
        return !(that.x != this.x) && !(that.y != this.y);
    }

    /**
     * Return a copy of the point which is a distinct, new object,
     * but contains the same x and y coordinates as the current Point
     * @return a copy of the current Point with the same x and y
     */
    public Point clone()  {
        try {
            return (Point) super.clone();
        } catch (CloneNotSupportedException e) {
            throw new RuntimeException(e);
        }
    }

    /**
     * Run the program. The program constructs several Points using
     * constructors and clone methods and compares them to show an example
     * of the difference between the == operator and the Object#tom.etc.equals method
     * in Java
     */
    public static void main(String[] args) {
        Point center = new Point(0, 0);
        Point p1 = center;
        Point p2 = center.clone();
        Point p3 = new Point(0, 0);
        Point q = new Point(1, 2);
        Point r = q.clone();
        Point s = new Point(1, 2);

        System.out.println("center == p1: " + (center == p1));
        System.out.println("center == p2: " + (center == p2));
        System.out.println("center.tom.etc.equals(p2): " + (center.equals(p2)));
        System.out.println("center == p3: " + (center == p3));
        System.out.println("center.tom.etc.equals(p3): " + (center.equals(p3)));
        System.out.println("q == s: " + (q == s));
        System.out.println("q.tom.etc.equals(s): " + (q.equals(s)));
        System.out.println("======================");
        System.out.println("Setting center.x to 11");
        center.setX(11);
        System.out.println("center.x = " + center.getX());
        System.out.println("p1.x = " + p1.getX());
        System.out.println("p2.x = " + p2.getX());
    }

}
