package tom.etc;

import java.util.Optional;
import java.util.function.Supplier;

public class ThreadingExample {

    public static <T> Optional<T> resolve(Supplier<T> chainSupplier) {
        try {
            T result = chainSupplier.get();
            return Optional.ofNullable(result);
        } catch ( NullPointerException e ) {
            return Optional.empty();
        }
    }


    static class Printer {
        public Printer(long id) {
            this.id = id;
        }

        public Printer() {
        }

        long id;

        public long getId() {
            return (id);
        }
    }

    static class USB {
        Printer printer;

        public USB() {
        }

        public USB(Printer printer) {

            this.printer = printer;
        }

        public Printer getPrinter() {
            return (printer);
        }
    }

    static class IO {
        public USB getUsb() {
            return usb;
        }

        public IO() {
        }

        public IO(USB usb) {

            this.usb = usb;
        }

        USB usb;
    }

    static class Computer {
        IO io;

        public IO getIo() {
            return (io);
        }

        public Computer() {
        }

        public Computer(IO io) {

            this.io = io;
        }
    }

    public static void main(String[] args) {

        Computer c1 = new Computer();
        Computer c2 = new Computer(new IO());
        Computer c3 = new Computer(new IO(new USB()));
        Computer c4 = new Computer(new IO(new USB(new Printer())));
        Computer c5 = new Computer(new IO(new USB(new Printer(12300))));

        Optional<Long> l1 = resolve(() -> c1.getIo().getUsb().getPrinter().getId());
        Optional<Long> l2 = resolve(() -> c2.getIo().getUsb().getPrinter().getId());
        Optional<Long> l3 = resolve(() -> c3.getIo().getUsb().getPrinter().getId());
        Optional<Long> l4 = resolve(() -> c4.getIo().getUsb().getPrinter().getId());
        Optional<Long> l5 = resolve(() -> c5.getIo().getUsb().getPrinter().getId());

        System.out.println(l1);
        System.out.println(l2);
        System.out.println(l3);
        System.out.println(l4);
        System.out.println(l5);
    }
}
