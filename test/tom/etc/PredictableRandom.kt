package tom.etc

import tom.utils.aggregation.BitVector
import tom.utils.iter.allIndicesOf
import tom.utils.math.isMultipleOf

fun main() {
    val arr = BitVector(20) { (it shr 1 xor 2 == 0) }

    val x = arr.toBooleanArray().toList()

    val indices = x.allIndicesOf(false)

    for (index in indices) {
        println(index)
    }

    if (30 isMultipleOf 2) {
        return
    }


    println(arr[3])
    println(arr)
    println(arr.trueCount)
    println(arr.falseCount)
    println(arr.toBitString())
    println(arr.toBigInteger())
}
