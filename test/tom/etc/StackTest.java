package tom.etc;

import java.util.Stack;

/**
 * @author Thomas Povinelli
 * Created 2018-Feb-21
 * In TomUtils
 */
public class StackTest {
    public static double evaluatePrefix(String expr) {
        Stack<Character> charStack = new Stack<Character>();
        Stack<Double> intStack = new Stack<Double>();

        // Note that this reverses the order of the input expr so that when we
        // pop from charStack, it comes out in reverse order.
        for (int indexExpr = 0; indexExpr < expr.length(); indexExpr++) {
            charStack.push(expr.charAt(indexExpr));
        }

        // Treat intStack as a buffer as well as the answer holder.
        while (!charStack.isEmpty()) {
            char c = charStack.pop();
            if (c == '+'){
                double second = intStack.pop();
                double first = intStack.pop();
                intStack.push(first + second);
            } else if (c == '-'){
                double second = intStack.pop();
                double first = intStack.pop();
                intStack.push(first - second);
            } else if (c == '*'){
                double second = intStack.pop();
                double first = intStack.pop();
                intStack.push(first * second);
            } else if (c == '/'){
                double second = intStack.pop();
                double first = intStack.pop();
                intStack.push(first / second);
            } else{
                intStack.push((double)Character.getNumericValue(c));
            }
        }
        return intStack.pop();
    }

    public static void main(String[] args) {
        String expr = "/93";
        double num = evaluatePrefix(expr);
        System.out.println(num);
    }
}
