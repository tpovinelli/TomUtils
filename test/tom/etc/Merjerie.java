package tom.etc;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Merjerie {

    public static <T extends Comparable<T>> List<T>
    mergesort(final List<T> l) {
        if (l.size() <= 1) {
            return l;
        }

        int mid = l.size() / 2;
        List<T> left = mergesort(new ArrayList<>(l.subList(0, mid)));
        List<T> right = mergesort(new ArrayList<>(l.subList(mid, l.size())));
        return merge(left, right);
    }

    private static <T extends Comparable<T>> List<T>
    merge(final List<T> left, final List<T> right) {

        final List<T> result = new ArrayList<>();
        final int stop = left.size() + right.size();
        int leftIndex = 0;
        int rightIndex = 0;

        for (int ignored = 0; ignored < stop; ignored++) {
            if (leftIndex >= left.size()) {
                result.addAll(new ArrayList<>(right.subList(rightIndex, right.size())));
                break;
            }
            if (rightIndex >= right.size()) {
                result.addAll(new ArrayList<>(left.subList(leftIndex, left.size())));
                break;
            }
            if (left.get(leftIndex).compareTo(right.get(rightIndex)) < 0) {
                result.add(left.get(leftIndex));
                leftIndex++;
            } else {
                result.add(right.get(rightIndex));
                rightIndex++;
            }
        }
        return result;
    }

    public static void main(String[] args) {
        List<Integer> l = Arrays.asList(3, 1, 0, 8, 9, 2, 6, 7);
        System.out.println(mergesort(l));
    }

}
