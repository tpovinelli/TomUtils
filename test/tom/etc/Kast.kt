package tom.etc

fun <T : Comparable<T>> mergesort(c: Collection<T>): Collection<T> {
    return mergesort(c.toList())
}

fun <T : Comparable<T>> mergesort(l: List<T>): List<T> {
    if (l.size <= 1) {
        return l
    }

    val mid = l.size / 2
    val left = mergesort(l.subList(0, mid))
    val right = mergesort(l.subList(mid, l.size))
    return merge(left, right)
}

fun <T : Comparable<T>> merge(left: List<T>, right: List<T>): List<T> {
    val result = mutableListOf<T>()
    val stop = left.size + right.size
    var leftIndex = 0
    var rightIndex = 0
    for (i in 0 until stop) {
        if (leftIndex >= left.size) {
            result.addAll(right.subList(rightIndex, right.size).toMutableList())
            break
        }
        if (rightIndex >= right.size) {
            result.addAll(left.subList(leftIndex, left.size).toMutableList())
            break
        }
        if (left[leftIndex] < right[rightIndex]) {
            result.add(left[leftIndex])
            leftIndex++
        } else {
            result.add(right[rightIndex])
            rightIndex++
        }
    }
    return result
}

fun main() {
    val s = listOf(1, 5, 2, 9, 0, 3, 6)
    println(mergesort(s))
}
