package tom.etc;

import org.jetbrains.annotations.Nullable;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.function.Consumer;
import java.util.function.Function;

/**
 * @author Thomas Povinelli
 * Created 2018-Feb-21
 * In TomUtils
 */
class   MissingAnimalJ extends AnimalJ {
    private static MissingAnimalJ INSTANCE;
    private static volatile int iCount = 0;

    static {
        INSTANCE = new MissingAnimalJ();
    }

    public static MissingAnimalJ getInstance() {
        return INSTANCE;
    }

    private MissingAnimalJ() {
        super(-1, "no name");
        int c = iCount + 1;
        if (c > 1) {
            throw new AssertionError("Cannot Instantiate Singleton MissingAnimalJ");
        }
        iCount = c;
    }
}

public class AnimalJ {
    private static int aId = 0;
    private static HashMap<Integer, AnimalJ> objs = new HashMap<>();

    public static Collection<AnimalJ> all() {
        return Collections.unmodifiableCollection(objs.values());
    }

    public static void main(String[] args) throws IllegalAccessException, InstantiationException, NoSuchMethodException, InvocationTargetException {
        AnimalJ a = AnimalJ.create("Dog");
        AnimalJ b = AnimalJ.find(3);

        Constructor<MissingAnimalJ> ctor = MissingAnimalJ.class.getDeclaredConstructor();
        ctor.setAccessible(true);
        MissingAnimalJ j = ctor.newInstance();

        System.out.println(a.id);
        System.out.println(b.id);
        if (b.exists()) {
            System.out.println(b.name);
        }
    }

    public static AnimalJ create(String name) {
        AnimalJ a = new AnimalJ(aId, name);
        objs.put(aId++, a);
        return a;
    }

    public static AnimalJ find(int id) {
        return objs.get(id) == null ? MissingAnimalJ.getInstance() : objs.get(id);
    }

    public String name;
    private int id;

    protected AnimalJ(int id, String name) {
        this.id = id;
        this.name = name;
    }

    public void ifExists(Consumer<AnimalJ> c) {
        if (this.exists()) {
            c.accept(this);
        }
    }

    public boolean exists() { return !(this instanceof MissingAnimalJ); }

    @Nullable
    public <R> R ifExistsMap(Function<AnimalJ, R> f) {
        if (this.exists()) {
            return f.apply(this);
        } else {
            return null;
        }
    }

    @Override
    public String toString() {
        return "AnimalJ{" +
                "name='" + name + '\'' +
                ", id=" + id +
                '}';
    }
}
