package tom.etc;

import tom.utils.aggregation.BitVector;

import java.util.List;
import java.util.function.Function;
import java.util.stream.Stream;

/**
 * @author Thomas Povinelli
 * Created 2017-Oct-19
 * In TomUtils
 */


public class Test {
    public static <T> T random(T[] array) {
        int length = array.length;
        int index = (int) (Math.random() * length);
        return array[index];
    }

    public static <T> T random(List<T> list) {
        int length = list.size();
        int index = (int) (Math.random() * length);
        return list.get(index);
    }

    public static <T> Stream<T> random(Stream<T> stream) {
        long length = stream.count();
        final long[] index = {(long) (Math.random() * length)};

        return Stream.of(stream.reduce((acc, val) -> {
            if (index[0] > 0) {
                acc = val;
                index[0]--;
            }
            return acc;
        }).get());

    }

    public static int random(int n) {
        return random(0, n);
    }

    public static double random(double x) {
        return Math.random() * x;
    }

    public static int random(int low, int high) {
        return (int) (Math.random() * (high - low) + low);
    }

    public static double random(double low, double high) {
        return Math.random() * (high - low) + low;
    }

    public static double random() {
        return Math.random();
    }

    public static void main(String[] args) {
        BitVector vector = new BitVector(20, (Function<Integer, Boolean>) integer -> (integer & 3) != 0);
        System.out.println(vector.getTrueCount());
        System.out.println(vector.getFalseCount());
        System.out.println(vector.getBitsSize());
        System.out.println(vector);

        String[] names = {"Thomas", "John", "Vincent", "Povinelli"};
        int width = 360;

        int r1 = random(width);
        String name = random(names);
        int r2 = random(-1, 1);
        double r4 = random(-1F, 1F);
        double r3 = random();
    }
}
