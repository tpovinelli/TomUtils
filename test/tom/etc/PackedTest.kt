package tom.etc

import tom.utils.math.Complex
import tom.utils.math.KComplex
import tom.utils.math.j
import tom.utils.math.plus


fun main() {

    val a = Complex(3.0, 2.0)
    val b = a.mul(3.0)
    val z = a.conjugate()
            .div(b.realComponent())
            .add(3.0)
            .mul(Complex(1.0, 1.0).negate().squared())
            .squared()
            .sub(Complex.ONE)

    val c = KComplex(3.0, 4.0)
    val d = !(c * 2.0)
    val e = -(d / KComplex(5.0, -1.0))
    val f = 3.0 + 4.0.j

    println(listOf(c, d, e, f))
    println(c == f)

}
