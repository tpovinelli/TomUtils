package tom.etc

/**
 * @author Thomas Povinelli
 * Created 2018-Feb-02
 * In TomUtils
 */

// many ext functions with parameters
// 30.tom.etc.format(padding='0', length=10, etc.)
// many typed DSL
// tom.etc.format(10.2) { padding = ' '; etc. }
// one generic DSL
// tom.etc.format(<any>) { type = Double.class, afterDecimal = 3.0 } -- same problems as % tom.etc.format, i.e. after decimal is not always useful
// BUilding
// Format(10).withPadding(' ').ofLength(5).local(Locale())

fun Int.format(leftJustify: Boolean = false, padding: Char? = null, length: Int? = null): String {
    var formatter = "%"
    formatter += if (leftJustify) "-" else ""
    formatter += padding ?: ""
    formatter += length ?: ""
    formatter += "d"
    return formatter.format(this)
}

fun Double.format(leftJustify: Boolean = false, padding: Char? = null, beforeDecimal: Int? = null, afterDecimal: Int? = null): String {
    var formatter = "%"
    formatter += if (leftJustify) "-" else ""
    formatter += padding ?: ""
    formatter += beforeDecimal ?: ""
    formatter += if (afterDecimal == null) "" else ".$afterDecimal"
    formatter += "f"
    return formatter.format(this)
}

fun main(args: Array<String>) {
    println(10.format(false, '0', 20))
    println(10.format(true, null, 20))
    println(22.585.format(true, ' ', 3, 1))
    println(22.585.format(true, ' ', 3))
    println(22.585.format())
}
