package tom.etc;

import sun.misc.Unsafe;

import java.lang.reflect.Field;
import java.util.Scanner;

/**
 * @author Thomas Povinelli
 * Created 1/7/18
 * In TomUtils
 */
public class UnsafeTesting {
    public static void main(String[] args) throws NoSuchFieldException, IllegalAccessException {
        Unsafe unsafe = getUnsafe();
        long address = unsafe.allocateMemory(4_000_000_000L);
        for (long i = 0; i < 4_000_000_000L; i++) {
            unsafe.putByte( address + i, (byte)i );
        }
        System.out.println(String.format("0x%08x", address));
        new Scanner(System.in).nextLine();
    }

    public static Unsafe getUnsafe(String fieldName) throws NoSuchFieldException, IllegalAccessException {
        Unsafe unsafe;
        Field field = Unsafe.class.getDeclaredField(fieldName);
        field.setAccessible(true);
        unsafe = (Unsafe) field.get(null);
        return unsafe;
    }


    public static Unsafe getUnsafe() throws NoSuchFieldException, IllegalAccessException {
        return getUnsafe("theUnsafe");
    }

    public static final class Nothing {
        public static Nothing INSTANCE = new Nothing();

        private Nothing() { }

        @Override
        public int hashCode() {
            return 0;
        }

        @Override
        public boolean equals(Object obj) {
            return obj == this;
        }

        @Override
        public String toString() {
            return "Nothing";
        }
    }
}
