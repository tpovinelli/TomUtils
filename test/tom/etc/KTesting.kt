package tom.etc

import tom.utils.geom.map

/**
 * @author Thomas Povinelli
 * Created 2017-Dec-22
 * In TomUtils
 */


fun main() {
//  val a = trying { Integer.parseInt("Not an integer") }
//  val b = try    { Integer.parseInt("not an integer") } catch (e: Exception) { null }
////  println(a)
//
//  val p = Point(3.0, 4.0)
//  val q = Point(0.0 to 0.0)
//  val r = Point(Point(Point(Point(Point(p)))))
//  val s = Point(Point2D.ZERO)
//
//  println(p.distanceToOrigin())
//  println(q.distanceToOrigin())
//  println(q.distanceTo(p))
//  println(p.midpointTo(q))
//  println(p.midpointTo(Point.origin))
//  println(Point.distanceBetween(0.0, 0.0, 3.0, 4.0))
//  println(Point.midpointBetween(p.x, p.y, q.x, q.y))
//  println(s.distanceTo(other = r))
    println((50.0).map(0.0, 100.0, 90.0, 350.0))
}
