package tom.etc;

/**
 * @author Thomas Povinelli
 * Created 2018-Jan-16
 * In TomUtils
 */

public enum Direction {
  NORTH("north"),
  SOUTH("south"),
  EAST("east"),
  WEST("west"),
  NORTH_WEST("north_west"),
  NORTH_EAST("north_east"),
  SOUTH_WEST("south_west"),
  SOUTH_EAST("south_east");

  private String value;

  Direction(String name) {
    this.value = name;
  }

  public Direction orthogonalDirection() {
    switch (this) {
      case NORTH:
        return SOUTH;
      case SOUTH:
        return NORTH;
      case EAST:
        return WEST;
      case WEST:
        return EAST;
      case NORTH_WEST:
        return SOUTH_EAST;
      case NORTH_EAST:
        return SOUTH_WEST;
      case SOUTH_EAST:
        return NORTH_WEST;
      case SOUTH_WEST:
        return NORTH_EAST;
      default:
        throw new RuntimeException("Invalid direction " + value);
    }
  }
}
