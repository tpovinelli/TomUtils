package tom.etc;

import org.jetbrains.annotations.Contract;
import org.jetbrains.annotations.NotNull;

import java.util.EmptyStackException;
import java.util.Scanner;
import java.util.Stack;

public class PrefixEvalUtil {
    public static void main(String[] args) {
        // use tilde (~) for negative numbers NOT minus (-)
        //    String expr = "+ 8 ~3";
        Scanner s = new Scanner(System.in);
        System.out.println("Use the following REPL to evalute prefix expressions. ");
        System.out.println("Operators supported are add (+), subtract (-), multiply (*), and divide (/)");
        System.out.println("Use tilde (~) attached to a number for negation. DO NOT USE minus (-) for negative numbers");
        while (true) {
            System.out.print("Enter a prefix expression or 'quit' to quit: ");
            String expr = s.nextLine();
            if (expr.equalsIgnoreCase("quit")) {
                break;
            }
            double num = evaluatePrefix(expr);
            System.out.println(num);
        }
    }


    public static double evaluatePrefix(@NotNull final String expr) {
        final Stack<Character> charStack = new Stack<>();
        final Stack<Double> doubleStack = new Stack<>();

        // Note that this reverses the order of the input expr so that when we
        // pop from charStack, it comes out in reverse order.
        for (int indexExpr = 0; indexExpr < expr.length(); indexExpr++) {
            charStack.push(expr.charAt(indexExpr));
        }

        // Treat doubleStack as a buffer as well as the answer holder.
        while (!charStack.isEmpty()) {
            final char c = charStack.pop();
            if (isOperator(c)) {
                double second, first;
                try {
                    first = doubleStack.pop();
                    second = doubleStack.pop();
                } catch (EmptyStackException e) {
                    throw new ArithmeticException("Invalid prefix expression: " + expr);
                }
                if (c == '+') {
                    doubleStack.push(first + second);
                } else if (c == '-') {
                    doubleStack.push(first - second);
                } else if (c == '*') {
                    doubleStack.push(first * second);
                } else if (c == '/') {
                    doubleStack.push(first / second);
                }
            } else if (isNumeric(c)) {
                charStack.push(c); // put back c
                doubleStack.push(satolf(charStack));
            } else if (c != ' ') {
                throw new IllegalArgumentException("Illegal character in expression: '" + c + "'");
            }
        }
        return doubleStack.pop();
    }

    @Contract(pure = true)
    private static boolean isOperator(final char c) {
        return c == '+' || c == '-' || c == '*' || c == '/';
    }

    @Contract(pure = true)
    private static boolean isNumeric(final char c) {
        return Character.isDigit(c) || c == '~' || c == '.' || c == 'e' || c == 'E';
    }

    public static double satolf(@NotNull Stack<Character> characterStack) {
        final StringBuilder v = new StringBuilder();
        while (!characterStack.empty() && isNumeric(characterStack.peek())) {
            Character d = characterStack.pop();
            if (d.equals('~')) {
                d = '-';
            }
            v.append(d);
        }
        return Double.parseDouble(v.reverse().toString());
    }

}
