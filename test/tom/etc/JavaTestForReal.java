package tom.etc;

import java.util.function.Consumer;
import java.util.function.Supplier;

class Uncheck {
    public static <T> Supplier<T> supplier(Supplier<T> supplier) {
        return () -> {
            try {
                return supplier.get();
            } catch (Exception e) {
                throw new RuntimeException(e);
            }
        };
    }

    public static <T> Consumer<T> consumer(Consumer<T> consumer) {
        return (T c) -> {
            try {
                consumer.accept(c);
            } catch (Exception e) {
                throw new RuntimeException(e);
            }
        };
    }

    public static Runnable runnable(Runnable runnable) {
        return () -> {
            try {
                runnable.run();
            } catch (Exception e) {
                throw new RuntimeException(e);
            }
        };
    }

    public static void wrap(Runnable runnable) {
        try {
            runnable.run();
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    public static void squash(Runnable runnable) {
        try {
            runnable.run();
        } catch (Exception ignored) {
        }
    }

    public static String getString() throws Exception {
        return "";
    }

    public static String something(int a, Supplier<String> s) {
        if (a == 0) {
            return s.get();
        } else {
            return "";
        }
    }

    public static void main(String[] args) {


    }
}
