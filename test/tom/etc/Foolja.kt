package tom.etc

/**
 * @author Thomas Povinelli
 * Created 2019-Jan-19
 * In TomUtils
 */

inline fun <reified T : Any?> isInstance(obj: Any?): Boolean {
    return T::class.java.isInstance(obj)
}

inline fun <reified T : Any?> safeDynamicCast(obj: Any?): T? {
    return if (isInstance<T>(obj)) dynamicCast(obj) else null
}

inline fun <reified T : Any?> dynamicCast(obj: Any?): T {
    return try {
        T::class.java.cast(obj)
    } catch (npe: NullPointerException) {
        throw TypeCastException("Cannot cast null to non nullable type")
    }
}

fun main(args: Array<String>) {
    val message: String = dynamicCast(null)
    if (message === null) {
        println("message is null")
        println(message is String)
    }
}

