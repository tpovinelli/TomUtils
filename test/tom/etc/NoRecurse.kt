package tom.etc

import tom.utils.delegates.BoundedValue
import tom.utils.delegates.Rounded

private fun <T : Any> T.sendConstructor(arguments: Array<out Any?>, types: Array<out Class<*>>): Any {
    return javaClass.getConstructor(*types).newInstance(arguments.zip(types).map { it.second.cast(it.first) })
}

fun <T : Any> T.send(message: String, vararg args: Any?): Any? {
    val a = args.map { it?.javaClass ?: java.lang.Object::class.java }.toTypedArray()
    return if (message == "new") {
        sendConstructor(args, a)
    } else {
        val method = javaClass.getDeclaredMethod(message, *a) ?: noSuchMethod(message)
        method.invoke(this, *args)
    }
}

private fun <T : Any> T.noSuchMethod(message: String): Nothing {
    throw NoSuchMethodException("No method named $message in ${this.javaClass}")
}


fun main() {

    println("Hello".send("length"))
    println("".send("new", "Hello World")?.send("length"))


    val r: Int by BoundedValue(30, 0, 100)
    var x: Double by Rounded(3.0, 0)
    x = 50.0 / 7.0
    println(x)

}
