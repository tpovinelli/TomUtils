package tom.etc

import tom.utils.file.containsFile
import tom.utils.iter.count
import tom.utils.iter.frequencies
import tom.utils.iter.onEachIndexed
import tom.utils.iter.uniqueElements
import tom.utils.string.tokens
import tom.utils.string.withoutPunctuation
import java.io.File

fun countWordsOfFile(filename: String): Map<String, Int> {
    return mapOf(*File(filename)
            .readText()
            .split("\n")
            .map { it.tokens(delim = " ") }
            .flatten()
            .map(String::toLowerCase)
            .map { it.withoutPunctuation() }
            .filter(String::isNotBlank)
            .onEach { println(it) }
            .frequencies()
            .entries
            .sortedByDescending { a -> a.value as Int? }
            .map { a -> a.toPair() }
            .toTypedArray())
}

fun countWords() {

    val words = File("hello.txt").readText()
            .split("\n")
            .map { it.split(" ") }
            .flatten()
            .map(String::toLowerCase)
            .map { it.withoutPunctuation() }
            .filter(String::isNotBlank)

    val x = words.size
    val y = words.uniqueElements().count()
    val freqs = words.frequencies().entries.sortedByDescending { a -> a.value as Int? }

    assert(words.frequencies().entries.map { it.value }.reduce { acc, entry -> acc + entry } == x)

    println("All words: $x\nUnique words: $y")
    print("Frequencies: $freqs")


}

fun metaSum(range: IntRange): Array<Long> {
    val result = Array<Long>(range.count) { 0 }
    range.onEachIndexed { idx, elt ->
        result[idx] = tailRecursiveSum(0..elt)
    }
    return result
}

fun recursiveSum(range: IntRange): Int = when {
    range.isEmpty() -> 0
    else            -> range.first + recursiveSum((range.first + 1)..range.last)
}

tailrec fun tailRecursiveSum(range: IntRange, acc: Long = 0): Long = when {
    range.isEmpty() -> acc
    else            -> tailRecursiveSum((range.first + 1)..range.last, acc + range.first)
}


fun main(args: Array<String>) {
    val root = File("test")
    val file = File("test${File.separator}Memtest.kt")
    if (file containsFile file) {
        println("$file in $root")
    }
}
