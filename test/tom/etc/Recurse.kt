package tom.etc

import tom.etc.Element.Companion.merge

open class Element(val name: String, val head: Element?, val dependents: List<Element>) {
    override fun toString(): String {
        val ret = name
        return if (dependents.isEmpty()) {
            ret
        } else {
            "[$ret, $dependents]"
        }
    }

    override fun equals(other: Any?): Boolean {
        if (other === this) {
            return true
        }
        if (other?.javaClass != javaClass) {
            return false
        }

        other as Element

        return other.name == name &&
                other.head == head &&
                other.dependents == dependents
    }

    override fun hashCode(): Int {
        var result = name.hashCode()
        result = 31 * result + (head?.hashCode() ?: 0)
        result = 31 * result + dependents.hashCode()
        return result
    }

    infix fun merge(b: Element): Element {
        return merge(this, b)
    }

    operator fun plus(b: Element): Element {
        return this merge b
    }

    companion object {
        @JvmStatic
        fun merge(a: Element, b: Element): Element {
            return Element(a.toString(), a, listOf(a, b))
        }
    }

}

object A : Element("A", null, listOf())
object B : Element("B", null, listOf())
object C : Element("C", null, listOf())
object D : Element("D", null, listOf())
object E : Element("E", null, listOf())
object F : Element("tom.etc.F", null, listOf())
object G : Element("tom.etc.G", null, listOf())
object H : Element("H", null, listOf())
object I : Element("I", null, listOf())
object J : Element("J", null, listOf())

fun main() {
    // infix functions are left associative by default which is the long form
    // by default chaining tom.etc.merge will chain (a tom.etc.merge b) tom.etc.merge c which makes
    // the long way of merging. Parentheses are needed for right associativity
    val left = ((((A merge B) merge C) merge D) merge E) merge F
    val right = A merge (B merge (C merge (D merge (E merge F))))
    val normal = A merge B merge C merge D merge E merge F
    val x = merge(merge(merge(A, B), C), merge(D, merge(merge(F, merge(G, H)), I)))
    val y = A + ((B + C) + ((D + E) + F)) + I + (G + H)
    println(x)
    println(y)
    println(left)
    println(right)
    println(normal)
    println(normal == left)
}
