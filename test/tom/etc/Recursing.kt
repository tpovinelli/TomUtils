package tom.etc

import tom.utils.iter.Index
import tom.utils.iter.partition
import tom.utils.string.size
import java.util.*

fun main() {
    val r = Random()
    val (small, medium, large) = r.doubles(50, 0.0, 1.0)
            .toArray()
            .toList()
            .partition(3) {
                return@partition when (it) {
                    in 0.0..0.33 -> Index.ZERO
                    in 0.34..0.66 -> Index.ONE
                    in 0.67..1.0 -> Index.TWO
                    else -> throwIllegalExpression("$it")
                }
            }
            .map { list -> list.joinToString { String.format("%.2f", it) } }

    println(small)
    println(medium)
    println(large)

    println("small=${small.size}, medium=${medium.size}, large=${large.size}")
}
