package tom.etc

fun Double.equals(other: Double, delta: Double) = Math.abs(this - other) <= delta

tailrec fun sqrt(x: Double, guess: Double = x / 2, delta: Double = 2e-11): Double {
    val other = x / guess
    return if (guess.equals(other, delta = delta)) {
        guess
    } else {
        sqrt(x, guess = ((other + guess) / 2))
    }
}

val Int.Companion.squared: (Int) -> Int get() = { it * it }
fun Int.Companion.pow(n: Int): (Int) -> Double = { Math.pow(it.toDouble(), n.toDouble()) }

fun main() {

    val a = 1.2
    val b = 1.201
    if (a.equals(b, 0.3)) {
        println("$a - $b < 0.3")
    }

    for (i in (1..25).toList().map(Int.squared)) {
        println(sqrt(i.toDouble()))
    }
}
