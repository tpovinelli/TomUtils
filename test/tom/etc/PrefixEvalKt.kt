package tom.etc

/**
 * @author Thomas Povinelli
 * Created 2019-Jan-12
 * In TomUtils
 */
import tom.utils.kotlin.trying
import java.util.*

fun main() {
    // use tilde (~) for negative numbers NOT minus (-)
    //    String expr = "+ 8 ~3";
    val s = Scanner(System.`in`)
    println("Use the following REPL to evaluate prefix expressions. ")
    println("Operators supported are add (+), subtract (-), multiply (*), and divide (/)")
    println("Use tilde (~) attached to a number for negation. DO NOT USE minus (-) for negative numbers")
    while (true) {
        print("Enter a prefix expression or 'quit' to quit: ")
        val expr: String? = s.nextLine()
        if (expr === null || expr.toLowerCase() == "quit") {
            break
        }
        val num = evaluatePrefix(expr)
        println(num)
    }
}


fun evaluatePrefix(expr: String): Double {
    val charStack = Stack<Char>()
    val doubleStack = Stack<Double>()

    // Note that this reverses the order of the input expr so that when we
    // pop from charStack, it comes out in reverse order.
    expr.forEach { charStack.push(it) }

    // Treat intStack as a buffer as well as the answer holder.
    while (charStack.isNotEmpty()) {
        val c = charStack.pop()!!
        if (c.isOperator()) {
            val first = trying { doubleStack.pop() } ?: throwIllegalExpression(expr)
            val second = trying { doubleStack.pop() } ?: throwIllegalExpression(expr)
            when (c) {
                '+' -> doubleStack.push(first + second)
                '-' -> doubleStack.push(first - second)
                '*' -> doubleStack.push(first * second)
                '/' -> doubleStack.push(first / second)
            }
        } else if (c.isNumeric()) {
            charStack.push(c) // put back c
            doubleStack.push(charStack.satoi())
        } else if (c != ' ') {
            throw IllegalArgumentException("Illegal character in expression: '$c'")
        }
    }
    return doubleStack.pop()
}

fun throwIllegalExpression(expr: String): Nothing {
    throw ArithmeticException("Invalid prefix expression: $expr")
}

fun Char.isOperator(): Boolean {
    return this == '+' || this == '-' || this == '*' || this == '/'
}

fun Char.isNumeric(): Boolean {
    return Character.isDigit(this) || this == '~' || this == 'e' || this == 'E' || this == '.'
}

fun Stack<Char>.satoi(): Double {
    val v = StringBuilder()
    while (isNotEmpty() && peek().isNumeric()) {
        var d: Char = pop()
        if (d == '~') {
            d = '-'
        }
        v.append(d)
    }
    return java.lang.Double.parseDouble(v.reverse().toString())
}


