package tom.etc;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class ParallelStreamTest {

    private static Random random;

    public static void main(String[] args) {
        List<String> strings = new ArrayList<>();
        for (int i = 0; i < 1000000; i++) {
            strings.add(randomString());
        }

        long start = System.currentTimeMillis();
        int sum = strings.stream().mapToInt(String::length).sum();
        long end = System.currentTimeMillis();
        System.out.println("Serial Sum = " + (end - start));
        System.out.println("sum { "+sum + " } \n");

        start = System.currentTimeMillis();
        sum = strings.parallelStream().mapToInt(String::length).sum();
        end = System.currentTimeMillis();
        System.out.println("Parallel Sum = " + (end - start));
        System.out.println("sum { "+sum + " } \n");

        start = System.currentTimeMillis();
        sum = strings.stream().mapToInt(String::length).distinct().sum();
        end = System.currentTimeMillis();
        System.out.println("Serial Distinct Sum = " + (end - start));
        System.out.println("sum { "+sum + " } \n");

        start = System.currentTimeMillis();
        sum = strings.parallelStream().mapToInt(String::length).distinct().sum();
        end = System.currentTimeMillis();
        System.out.println("Parallel Distinct Sum = " + (end - start));
        System.out.println("sum { "+sum + " } \n");

    }

    private static String randomString() {
        random = new Random();
        StringBuilder result = new StringBuilder();
        for (int i = 0; i < 5 + random.nextInt(5); i++) {
            result.append(Character.forDigit(random.nextInt(127), 10));
        }
        return result.toString();
    }


}
