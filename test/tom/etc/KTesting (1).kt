package tom.etc

/**
 * @author Thomas Povinelli
 * Created 2017-Dec-22
 * In TomUtils
 */


fun throwInvalidYear(ys: String): Nothing {
    throw InvalidDateException("Invalid year $ys")
}

fun throwInvalidMonth(ms: String): Nothing {
    throw InvalidDateException("Invalid month $ms")
}

fun throwInvalidDay(ys: String): Nothing {
    throw InvalidDateException("Invalid day $ys")
}

object KTesting {

    @JvmStatic
    fun main(args: Array<String>) {
        val input = "2012-18-01"
        val components = input.split("-")
        val (yearString, monthString, dayString) = components
        val year = yearString.toIntOrNull()
                ?: throwInvalidYear(yearString)
        val month = monthString.toIntOrNull()
                ?: throwInvalidMonth(monthString)
        val day = dayString.toIntOrNull()
                ?: throwInvalidDay(dayString)

        if (year < 1970 || year > 2200) {
            throwInvalidYear("(out of bounds) $yearString")
        }

    }
}

class InvalidDateException(s: String): Exception(s)
