import java.util.*
import kotlin.math.sqrt

class PrimeSieveJava(private val sieveSize: Int) {
    // Java has a BitSet class included but switching to a boolean array of size improves performance by a lot
    // This brings the limitation of the sieve only being able to test numbers up to Integer.MAX_VALUE - 2 (Requested array size exceeds VM limit)
    private val dataSet: BitSet = BitSet(sieveSize)
    fun countPrimes(): Int = dataSet.cardinality()

    fun validateResults(): Boolean = VALIDATION_DATA.getOrDefault(sieveSize, -1) == countPrimes()

    @Suppress("NOTHING_TO_INLINE")
    private inline fun getBit(index: Int): Boolean = dataSet[index]

    @Suppress("NOTHING_TO_INLINE")
    private inline fun clearBit(index: Int) = dataSet.clear(index)

    fun runSieve() {
        var factor = 3
        val q = sqrt(sieveSize.toDouble()).toInt()
        while (factor < q) {
            for (num in factor..sieveSize) {
                if (getBit(num)) {
                    factor = num
                    break
                }
            }
            var num = factor * factor
            while (num <= sieveSize) {
                clearBit(num)
                num += factor * 2
            }
            factor += 2
        }
    }

    fun printResults(showResults: Boolean, duration: Double, passes: Int) {
        if (showResults) {
            print("2, ")
        }
        var count = 1
        for (num in 3..sieveSize) {
            if (getBit(num)) {
                if (showResults) {
                    print("$num, ")
                }
                count++
            }
        }
        if (showResults) {
            println()
        }
        println(
            "Passes: $passes, Time: $duration, Avg: ${duration / passes}, Limit: $sieveSize, Count: $count, Valid: ${validateResults()}",
        )

        // Following 2 lines added by rbergen to conform to drag race output format
        println()
        System.out.printf("MansenC;%d;%f;1;algorithm=base,faithful=yes\n", passes, duration)
    }
}

val VALIDATION_DATA: Map<Int, Int> = mapOf(
    10 to 4,
    100 to 25,
    1000 to 168,
    10000 to 1229,
    100000 to 9592,
    1000000 to 78498,
    10000000 to 664579,
    100000000 to 5761455
)

fun main() {
    val start = System.currentTimeMillis()
    var passes = 0
    var sieve: PrimeSieveJava? = null
    while (System.currentTimeMillis() - start < 10000) {
        sieve = PrimeSieveJava(1000000)
        sieve.runSieve()
        passes++
    }
    val delta = System.currentTimeMillis() - start
    sieve?.printResults(false, delta / 1000.0, passes)
}