import tom.utils.reflection.CheckCallerMethod
import tom.utils.reflection.checkCallerClass
import kotlin.contracts.ExperimentalContracts


class Something {
    fun test() {
        checkCallerClass(Something::class.java, "Cannot call from self class", method = CheckCallerMethod.specifyDenied)
    }
}

@ExperimentalContracts
fun main() {

}
