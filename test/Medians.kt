import tom.utils.iter.bounds
import tom.utils.math.collapse
import tom.utils.math.median
import tom.utils.math.mode
import tom.utils.string.lengthened

/**
 * @author Thomas Povinelli
 * Created 10/22/19
 * In TomUtils
 */

fun main() {
    val l = listOf(4, 2, 3, 9, 1, 8, 18, 2, 3, 6, 1, 1)
    val a = listOf("hello", "world", "there", "once", "again")

    val aMedian = a.median
    val lMedian = l.median
    println("Bounds".lengthened('=', 50))
    println(a.bounds)
    println(l.bounds)

    println("Modes".lengthened('=', 50))
    println(a.mode)
    println(l.mode)

    println("A Median".lengthened('=', 50))
    println(aMedian)
    println(aMedian.fold({ it }, { i, j -> i + j }))
    println(aMedian.each({ it.median }, { it.collapse(String::plus) }))

    println("L Median".lengthened('=', 50))
    println(lMedian)
    println(lMedian.fold({ it }, { i, j -> i + j }))
    println(lMedian.each({ it.median }, { it.collapse() }))
}
