import tom.utils.time.timeOnce
import java.math.BigInteger

fun <T, U> memoize(fn: ((T) -> U, T) -> U): (T) -> U {
    val cache = mutableMapOf<T, U>()
    var result: (T) -> U = { TODO() }
    result = returnValue@{ parameter ->
        val cached = cache[parameter]
        if (cached != null) return@returnValue cached
        val calculated = fn(result, parameter)
        cache[parameter] = calculated
        return@returnValue calculated
    }
    return result
}

fun rfib(n: BigInteger): BigInteger {
    return if (n <= 1.toBigInteger()) 1.toBigInteger()
    else rfib(n - 1.toBigInteger()) + rfib(n - 2.toBigInteger())
}

val mfib: (BigInteger) -> BigInteger = memoize { function, arg ->
    return@memoize if (arg <= 1.toBigInteger()) 1.toBigInteger()
    else function(arg - 1.toBigInteger()) + function(arg - 2.toBigInteger())
}

fun main() {

    timeOnce {
        rfib(38.toBigInteger())
    }.also { println(it) }

    timeOnce {
        mfib(107.toBigInteger())
    }.also { println(it) }
}
