/**
 * @author Thomas Povinelli
 * Created 11/3/19
 * In TomUtils
 */
public class CastTest {
    public static void main(String[] args) {
        Object s = "Hello";
        System.out.println(CastTest.<String>as(s));
    }

    public static <T> T as(Object any) {
        return (T) any;
    }
}
