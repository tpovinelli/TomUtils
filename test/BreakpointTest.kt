class EnvironmentVariable(val value: String?) {
    val isTrue: Boolean
        get() = this.value?.trim()?.equals("true", ignoreCase = true) ?: false
    val isFalse: Boolean get() = !this.isTrue
    val isSet: Boolean get() = value != null
    val hasValue: Boolean get() = !value.isNullOrBlank()

    fun contains(subString: String): Boolean = value?.contains(subString) == true
    fun contains(regex: Regex): Boolean = value?.contains(regex) == true

    fun matches(regex: Regex): Boolean = value?.matches(regex) == true
}

object Environment {
    operator fun get(key: String): EnvironmentVariable = EnvironmentVariable(System.getenv(key))
    val all
        get() = System.getenv().entries.associate {
            (it.key ?: "null") to EnvironmentVariable(it.value)
        }
}


object BreakpointTest {

    inline fun <reified T> Any.isA(): Boolean {
        return this::class.java == T::class.java
    }

    inline fun <reified T> Any?.coerce(): T {
        return T::class.java.cast(this)
    }

    inline fun <reified T> Any?.tryCoerce(): T? {
        return if (this?.isA<T>() == true) this.coerce<T>() else null
    }


    @JvmStatic
    fun main(args: Array<String>) {
        val a: Any = "Hello"

        println(a.isA<Int>())
        println(a.isA<String>())
        println(a.coerce<String>())
        println(a.tryCoerce<String>())
        println(a.tryCoerce<Int>())

    }
}
